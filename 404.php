<?
if ($_SERVER['DOCUMENT_URI'] == "/404.php") {
    $_SERVER['REQUEST_URI'] = $_SERVER['DOCUMENT_URI'];
}
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Страница не найдена");?>
<div class="page-404">
	<div class="question-icon">
		<img src="<?=SITE_TEMPLATE_PATH?>/images/question-icon.png" alt="К сожалению, запрашиваемая Вами страница не найдена." title="К сожалению, запрашиваемая Вами страница не найдена."/>
	</div>
	<h1>404</h1>
	<p>К сожалению, запрашиваемая Вами страница не найдена.</p>
	<p>Задайте интересующий Вас запрос с помощью формы поиска или оставьте сообщение в форме обратной связи</p>
</div>
<div class="clear"></div>
<?$APPLICATION->IncludeFile(
		SITE_TEMPLATE_PATH.'/_include_areas_/header.search.php',
		array(),
		array('MODE'=>'html', 'SHOW_BORDER'=>true)
	);
$APPLICATION->IncludeFile(
	SITE_TEMPLATE_PATH.'/_include_areas_/large.search.php',
	array(),
	array('MODE'=>'html', 'SHOW_BORDER'=>true)
);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>