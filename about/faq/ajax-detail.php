<?php
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
$id=intval($_REQUEST['id']);
$dbRes=CIBlockElement::GetByID($id);
if($arRes=$dbRes->GetNext()):?>
	<div class="ajax-faq-detail">
		<div class="scroll">
			<h2><?=$arRes['NAME']?></h2>
			<div class="detail"><?=$arRes['PREVIEW_TEXT']?></div>
		</div>
	</div>
<?endif;?>