﻿<?php
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$action=htmlspecialchars($_REQUEST['action']);
if(!$action):?>
	<form id="call-back" onsubmit="yaCounter23212990.reachGoal('call_feedback'); yaCounter23212990.reachGoal('all');return true;">
		<div class="call-back-block">
			<h2>Заказ обратного звонка</h2>
			<div class="result"></div>
			<table>
				<tr>
					<td class="f-td">Имя:</td>
					<td><input type="text" name="name" id="cName" placeholder="Имя" />
					</td>
				</tr>
				<tr>
					<td class="f-td">Телефон:</td>
					<td><input type="text" name="phone" id="cPhone" class="fPhone" placeholder="Телефон"/></td>
				</tr>
			</table>
			<table class="extendForm" style="display:none">
				<tr>
					<td class="f-td">Эл. почта:</td>
					<td><input type="text" name="email" id="cMail" class="fMail ignore" placeholder="E-mail" /></td>
				</tr>
				<tr>
					<td class="f-td">Сообщение:</td>
					<td><textarea class="fMsg ignore" name="message" placeholder="Сообщение"></textarea></td>
				</tr>
			</table>

			<div class="detail-form-on more-on extendOn">
				<span class="on extendFormOn">Расширенная форма</span>
				<span class="off extendFormOff">Обычная форма</span>
			</div>
			<div class="clear"></div>
			<input class="submit" type="submit" value="Заказать" />
		</div>
	</form>
	<script>
		$("#call-back").staFeedback();
	</script> <?
elseif($action=='send2'):
	$name		=	htmlspecialchars($_REQUEST['name']);
	$phone		=	htmlspecialchars($_REQUEST['phone']);
	$mail		=	htmlspecialchars($_REQUEST['email']);
	$message	=	htmlspecialchars($_REQUEST['message']);
	
	$mess		=	"Имя: " . $name . "\r\n";
	$mess		.=	"Телефон: " . $phone . "\r\n";
	
	if($mail)
		$mess .= "Эл. почта: " . $mail . "\r\n";
	if($message)
		$mess .= "Сообщение: " . $message . "\r\n";
		
	$arSend=array("TEXT"=>$mess);
	CEvent::Send('CALLBACK2',SITE_ID,$arSend);
endif;?>
