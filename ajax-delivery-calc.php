<?php
	define("NO_KEEP_STATISTIC", true);
	define("NOT_CHECK_PERMISSIONS", true);
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
	$submit	= htmlspecialchars($_REQUEST['submit']);
	$city	= htmlspecialchars($_REQUEST['city']);
	$weight = htmlspecialchars((int)$_REQUEST['weight']);
	$volume	= htmlspecialchars((int)$_REQUEST['volume']);
	
	if(!$submit):?>
	<div class="ajax-delivery-calc">
		<h1>Расчет доставки</h1>
		<form id="ajax-delivery-calc-form" onsubmit="yaCounter23212990.reachGoal('calc'); return true;">
			<div class="delivery-calc__subtitle">Выберите город отправитель в Китае</div>
			
			<div class="delivery_calc__selectWrp">
				<?$APPLICATION->IncludeFile(
					SITE_TEMPLATE_PATH.'/_include_areas_/calc.select.php',
					array(),
					array('MODE'=>'php','SHOW_BORDER'=>false)
				);?>
				<div class="delivery_calc__curency">
					<div class="delivery-calc__usd">USD = <span class="delivery-calc__usd-rate"></span> руб.</div>
					<div class="delivery-calc__yuan">CNY = <span class="delivery-calc__yuan-rate"></span> руб.</div>
				</div>
				<div class="clear"></div>
			</div>
			
			<div class="delivery-calc__subtitle">Укажите массогабаритные характеристики груза:</div>
			
			<label class="delivery-calc__inputWrp">
				<span class="delivery-calc__name">Вес, кг</span>
				<input class="delivery-calc__weight delivery-calc__input" type="text" name="weight" placeholder="Вес, кг"/>
			</label>
			
			<label class="delivery-calc__inputWrp fl-r">
				<span class="delivery-calc__name">Объем, м<sup>3</sup></span>
				<input class="delivery-calc__volume delivery-calc__input" type="text" name="volume" placeholder="Объем, м3"/>
			</label>
			
			<div class="clear"></div>
				
			<div class="delivery-calc__switcher">
				<label class="delivery-calc__radio">
					<input type="radio" checked="checked" name="contract" value="our"> Наш контракт
				</label>
				
				<label class="delivery-calc__radio">
					<input type="radio" name="contract" value="your"> Ваш контракт
				</label>
			</div>
			
			<input class="delivery-calc__submit" type="submit" name="submit" id="submit" value="Рассчитать"/>
			
			<div class="delivery-calc__totalPrice">
				<input class="delivery-calc__total delivery-calc__input totalRub" type="text" name="total" disabled="disabled" placeholder="Сумма:"/> Руб.
				<input class="delivery-calc__total delivery-calc__input totalUSD" type="text" name="total" disabled="disabled" placeholder="Сумма:"/> USD.
			</div>
		</form>

		<div class="delivery-calc__notice">
			Расчет произведен из города "<span class="delivery-calc__senderCity"></span>" в Китае до г. Новосибирска исходя из ставки за <span class="delivery-calc__rate"></span>
			для <span class="delivery-calc__cargoType"></span>. Сумма расчета является предварительной и не включает таможенных платежей.
		</div>

		<div class="clear"></div>

		<div id="delivery-calc__feedback" style="display: none">
			<form method="POST" class="delivery-calc__feedback-form" id="calc-feedback-form" action="/services/emailsend.php" enctype="multipart/form-data" onsubmit="yaCounter23212990.reachGoal('calc_lead'); yaCounter23212990.reachGoal('all');return true;">

				<div class="delivery-calc__note">
					Чтобы сформировать полный расчет поставки с учетом особенностей Вашей товарной группы и таможенных платежей:
				</div>
				
				<label class="delivery-calc__description-wrap">
					<div class="delivery-calc__subtitle">
						Введите описание груза, комментарий к поставке и код ТНВЭД, если известен:
					</div>
					<textarea class="delivery-calc__description" name="message" placeholder="Описание груза, комментарий к поставке и код ТНЭД, если известен"></textarea>
				</label>
				
				<table width="100%" cellpadding="6" cellspacing="0">
					<tr>
						<td width="250" class="delivery-calc__hr">
							<div class="delivery-calc__subtitle">Добавьте файлы при необходимости:</div>
						</td>
						<td width="*" class="delivery-calc__hr">
							<div class="delivery-calc__files">
								<div class="delivery-calc__file">
									<input id="file_upload" name="filename[]" type="file"/>
								</div>
							</div>
							<div class="delivery-calc__moreFiles">Прикрепить еще..</div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div class="delivery-calc__subtitle contacts">Заполните контактные данные</div>
						</td>
					</tr>
					<tr>
						<td class="delivery-calc__subtitle">Как к Вам обращаться?</td>
						<td>
							<label>
								<div>Введите имя:</div>
								<input type="text" name="name" placeholder="Введите имя" class=""/>
							</label>
						</td>
					</tr>
					<tr>
						<td>
							<label>
								<div>Email:</div>
								<input type="text" placeholder="e-mail" name="email" class=""/>
							</label>
						</td>
						<td>
							<label>
								<div>Телефон:</div>
								<input type="text" name="phone" placeholder="Телефон" class=""/>
							</label>
						</td>
					</tr>
					<tr>
						<td>
							<div class="delivery-calc__subtitle">Мы расчитаем поставку в течение 1 рабочего дня</div>
						</td>
						<td>
							<input type="submit" class="but-default" name="submit" value="Отправить заявку"/>
						</td>
					</tr>
				</table>
				<input type="hidden" name="from" class="delivery-calc__hiddenFrom"/>
				<input type="hidden" name="totalUSD" class="delivery-calc__totalUSD"/>
				<input type="hidden" name="totalRUB" class="delivery-calc__totalRUB"/>
				<input type="hidden" name="hiddenVolume" class="delivery-calc__hiddenVolume"/>
				<input type="hidden" name="hiddenWeight" class="delivery-calc__hiddenWeight"/>
				<input type="hidden" name="hiddenContract" class="delivery-calc__hiddenContract"/>
			</form>
		</div>
	</div>

	<?$APPLICATION->IncludeFile(
		SITE_TEMPLATE_PATH . "/js/deliveryCalc.js.php",
		array(),
		array('MODE'=>'php','SHOW_BORDER'=>false)
	);
	endif;
?>

