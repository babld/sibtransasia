<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.slimscroll.min.js" type="text/JavaScript"></script>
	<ul class="side-navigation">
		<?foreach($arResult as $arItem):?>
			<?if($arItem["SELECTED"]):?>
			<li class="current side-navigator-current"><a href="<?=$arItem["LINK"]?>"><?=$arItem['TEXT']?></a></li>
			<?else:?>
			<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
			<?endif;?>
		<?endforeach?>
	</ul>
	<?$dir=explode('/', $APPLICATION->GetCurDir());

	# придумать элегантней
	if($dir[1] != 'about') : ?>
		<script>
			$(function(){
				$('.side-navigation').slimScroll({
					height: '700px',
					railVisible: true,
					width: 219,
					distance: '0px',
					size: '12px',
					color: '#333',
					railColor: '#ccc',
					alwaysVisible: true,
					<?if(!empty($dir[2])) {?>
						start: $('.side-navigator-current'),
					<?}?>
					railOpacity: 1,
					wheelStep: 10
				});
			});
		</script>
	<? endif; ?>
<?endif?>