<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(is_array($arResult['PROPERTIES']['PICTURES']['VALUE']) AND count($arResult['PROPERTIES']['PICTURES']['VALUE'])>0):?>
<div class="galery">
	<?
	$arResult['PROPERTIES']['PICTURES']['VALUE']=array_slice($arResult['PROPERTIES']['PICTURES']['VALUE'],0,4);
	$k=0;
	foreach($arResult['PROPERTIES']['PICTURES']['VALUE'] as $picId):
		$arPic=CFile::ResizeImageGet($picId,array("width"=>140,"height"=>151),BX_RESIZE_IMAGE_PROPORTIONAL,false);
		$BigPic=CFile::GetFileArray($picId);
		?>
		<a href="<?=$BigPic['SRC']?>" rel="lightbox" title="<?=$arResult['PROPERTIES']['PICTURES']['DESCRIPTION'][$k]?>"><img src="<?=$arPic['src']?>" alt="<?=$arResult['PROPERTIES']['PICTURES']['DESCRIPTION'][$k]?>" /></a>
	<?
	$k++;
	endforeach;?>
</div>
<?endif;?>

<h2><?=$arResult['NAME']?></h2>

<?if($arResult['PROPERTIES']['POSITION']['VALUE']=='Верх'):?>
	<?$APPLICATION->IncludeFile(
		SITE_TEMPLATE_PATH.'/_include_areas_/photo.gallery.php',
		array("arResult" => $arResult),
		array('SHOW_BORDER'=>true)
	);?>
<?endif;?>

<?=$arResult['DETAIL_TEXT']?>

<?if($arResult['PROPERTIES']['POSITION']['VALUE']=='Низ'):?>
	<?$APPLICATION->IncludeFile(
		SITE_TEMPLATE_PATH.'/_include_areas_/photo.gallery.php',
		array("arResult" => $arResult),
		array('SHOW_BORDER'=>true)
	);?>
<?endif;?>