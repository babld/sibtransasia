<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<select name="city" class="delivery-calc__cities">
	<?foreach($arResult["ITEMS"] as $arItem):
		$price_lk_l20m3	= $arItem['PROPERTIES']['price_lk_l20m3']['VALUE'];
		$price_lk_g20m3 = $arItem['PROPERTIES']['price_lk_g20m3']['VALUE'];
		$price_hk_l20t	= $arItem['PROPERTIES']['price_hk_l20t']['VALUE'];
		$price_hk_g20t	= $arItem['PROPERTIES']['price_hk_g20t']['VALUE'];
		$price_wp		= $arItem['PROPERTIES']['price_wp']['VALUE'];?>
		<option class="no-active"
			data-price_lk_l20m3	= "<?=$price_lk_l20m3?>"
			data-price_lk_g20m3	= "<?=$price_lk_g20m3?>"
			data-price_hk_l20t	= "<?=$price_hk_l20t?>"
			data-price_hk_g20t	= "<?=$price_hk_g20t?>"
			data-price_wp		= "<?=$price_wp?>"><?=$arItem['NAME']?></option>
	<?endforeach;?>
</select>
