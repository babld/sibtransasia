<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<ul id="offices">
<?
$i=0;

foreach($arResult["ITEMS"] as $arItem):
	$i++;
	$pictures = $arItem['PROPERTIES']['PICTURES']['VALUE'];
	$mapCoords = $arItem["PROPERTIES"]["MAP"]["VALUE"];?>
	<li id="<?=$arItem['CODE']?>">
		<div class="contactsLeftCol">
			<div class="info">
				<h2><?=$arItem['PROPERTIES']['TYPE']['VALUE']?>: <?=$arItem['PROPERTIES']['COUNTRY']['VALUE']?>, <?=$arItem['NAME']?></h2>
				<ul class="location">
					<li class="address"><span>Адрес: </span><?=$arItem['PROPERTIES']['ADDRESS']['VALUE']?></li>
					<li><span>Телефон: </span> <?=$arItem['PROPERTIES']['PHONE']['VALUE']?></li>
				</ul>
			</div>
			
			<script type="text/javascript">
				function init () 
				{
					var myMap = new ymaps.Map("map<?=$arItem['ID']?>", 
					{
						center: [<?=$mapCoords?>],
						zoom: 16
					});
					myMap.controls.add('zoomControl').add('typeSelector').add('mapTools');

					myPlacemark = new ymaps.Placemark([<?=$mapCoords?>]);
					myMap.geoObjects.add(myPlacemark);
				}
				ymaps.ready(init);
			</script>
			<div class="contact-yandex-map" id="map<?=$arItem['ID']?>"></div>
		</div>
		<?if(is_array($pictures) AND count($pictures)>0):?>
			<div class="photo box-shadow">
				<?
				$j = 0;
				foreach($pictures as $picId):
					if($j > 2) break; /* проверка на то чтобы небыло больше 3х картинок */
					$arPic=CFile::ResizeImageGet($picId,array("width"=>115,"height"=>65),BX_RESIZE_IMAGE_EXACT,false);
					$arLarge=CFile::ResizeImageGet($picId,array("width"=>373,"height"=>302),BX_RESIZE_IMAGE_EXACT,false);
					$arBPic=CFile::GetFileArray($picId);?>

					<?if($j == 0): ?>
						<div class="large-photo">
							<a href="<?=$arBPic['SRC']?>" rel="gallery-<?=$i?>" class="fancybox">
								<img src="<?=$arLarge['src']?>" alt=" " />
							</a>
						</div>
					<?endif;?>

					<a href="<?=$arBPic['SRC']?>" rel="gallery-<?=$i?>" class="cLargeImg <?=($j == 0)?"":"fancybox"?>" data-bImage="<?=$arLarge['src']?>">
						<img src="<?=$arPic['src']?>" class="contactThumbImage <?=($picId === end($pictures))?"last":""?>"/>
					</a>

				<? $j++;
				endforeach;?>
				<div class="clear"></div>
			</div>
		<?endif;?>
		<div class="clear"></div>
	</li>
<?endforeach;?>
</ul>
