<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<ul class="faq-list">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<li class="no-active">
		<h3><a href="#"><?=$arItem['NAME']?></a></h3>
		<div class="panel"><?=$arItem['PREVIEW_TEXT']?></div>
	</li>
<?endforeach;?>
</ul>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><?=$arResult["NAV_STRING"]?><?endif;?>