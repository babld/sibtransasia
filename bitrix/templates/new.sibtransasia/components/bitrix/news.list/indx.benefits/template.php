<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<h2>Преимущества</h2>
<div id="benefits" class="box-shadow">
	<div id="benefits_image">
	<?
	$i=0;
	foreach($arResult["ITEMS"] as $arItem):
		$i++;
		if($i==1):
			$Text=$arItem['PREVIEW_TEXT'];
		endif;
		$arI[]=$arItem;
		?>
		<img id="benefits-<?=$i?>" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>" />
	<?endforeach;?>
		<div id="benefits_description"><span><?=$Text?></span></div>
	</div>
	<ul>
		<?
		$i=0;
		foreach($arI as $arItem):
			$i++; ?>
			<li>
				<span data-position="#benefits-<?=$i?>">
					<?=$arItem['NAME']?>
					<i>
						<?=$arItem['PREVIEW_TEXT']?>
					</i>
				</span>
			</li>
		<?endforeach;?>
	</ul>
	<div class="clear"></div>
</div>