<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<ul id="offices">
<?
$i=0;
foreach($arResult["ITEMS"] as $arItem):
$i++;?>
	<li id="<?=$arItem['CODE']?>">
		<div class="info">
			<h2><?=$arItem['NAME']?></h2>
			<ul>
				<li><?=$arItem['DETAIL_TEXT']?></li>				
			</ul>			
		</div>		
	</li>
<?endforeach;?>
</ul>