<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="managers" class="manager-block">
	<?
	$i=0;
	foreach($arResult["ITEMS"] as $arItem):
		$Mail = $arItem['PROPERTIES']['EMAIL']['VALUE'];
		
		$j = 0;
		$kodermail = '';
		for($j = 0; $j < strlen($Mail); $j++) {
			$kodermail .= '&#' . ord(substr($Mail, $j, 1)) . ';';
		}
		$i++;
		$res=CIBlockElement::GetByID($arItem['PROPERTIES']['OFFICE']['VALUE']);
		$ar_res=$res->GetNext();?>
		<div class="manager box-shadow" id="<?=$ar_res['CODE']?>-i">
			<div class="manager-photo">
				<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>" />
			</div>
			<div class="manager-detail">
				<ul>
					<li>Представитель</li>
					<li class="name"><?=$arItem['NAME']?></li>
					<li><span class="phone"></span><?=$arItem['PROPERTIES']['PHONE']['VALUE']?></li>
					<? if(!empty($arItem['PROPERTIES']['MOBILE']['VALUE'])) : ?>
						<li>
							<span class="mobile">
								<?=$arItem['PROPERTIES']['MOBILE']['VALUE']?>
							</span>
						</li>
					<? endif; ?>
					<li>
						<span class="mail"></span>
						<a href="mailto:<?=$kodermail?>">
							<?=$kodermail?>
						</a>
					</li>
					
					
					<li><span class="skype"></span><a href="callto:<?=$arItem['PROPERTIES']['SKYPE']['VALUE']?>"><?=$arItem['PROPERTIES']['SKYPE']['VALUE']?></a></li>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
	<?endforeach;?>
</div>
