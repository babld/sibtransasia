<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="rubric-pre-block">
	<h2 class="rubric-pre-title">Что мы возим</h2>
	<div class="rubric-pre-text">Компания СибирьТрансАзия занимается доставкой любых товаров из Китая.
	Благодаря большому опыту в сфере доставки и таможенного оформления самых различных грузов,
	работа с товаром, который еще не приходилось доставлять и растамаживать, не представляет для нас проблем.
	В разделе "Что мы возим" Вы можете ознакомиться с материалами по некоторым товарными группам,
	которые мы доставляем регулярно нашим клиентам по всей России.</div>
</div>
<div id="articles_listing">
<?foreach($arResult["ITEMS"] as $arItem):
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="article_listing">
		<?if($arItem['PREVIEW_PICTURE']['SRC']):?>
			<div class="article-announce-img">
				<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="" />
			</div>
		<?endif;?>
		<h2>
			<a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a>
		</h2>
		<div class="article-lead">
            <?=$arItem['PREVIEW_TEXT']?>
		</div>
		<div class="article-more-button">
			<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="button-style">Подробнее</a>
		</div>
		
	</div>
<?endforeach;?>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><?=$arResult["NAV_STRING"]?><?endif;?>