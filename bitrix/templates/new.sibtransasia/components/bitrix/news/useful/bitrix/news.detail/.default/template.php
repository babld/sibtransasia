<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="breadcrumbs"><a href="/useful/">Полезное</a> / <?=$arResult['NAME']?></div>
<h1><?=$arResult['NAME']?></h1>

<?if($arResult['PROPERTIES']['POSITION']['VALUE']=='Верх'):?>
<?if(count($arResult['PROPERTIES']['GALLERY']['VALUE'])>0 AND is_array($arResult['PROPERTIES']['GALLERY']['VALUE'])):?>
<div class="slider-block">
	<div class="big-pic">
		<?
		$i=0;
		foreach($arResult['PROPERTIES']['GALLERY']['VALUE'] as $picId):
		$arBigPic=CFile::ResizeImageGet($picId,Array("width"=>620,"height"=>500),BX_RESIZE_IMAGE_PROPORTIONAL,true);
		$arBig=CFile::GetFileArray($picId);
		?>
		<div class="item pic-<?=$i?>">
			<div class="over" style="width:<?=$arBigPic['width']?>px;">
				<a href="<?=$arBig['SRC']?>" class="fancybox" title="<?=$arResult['PROPERTIES']['GALLERY']['DESCRIPTION'][$i]?>" rel="useful-gallery"><img src="<?=$arBigPic['src']?>" alt="" /></a>
				<div class="name"><?=$arResult['PROPERTIES']['GALLERY']['DESCRIPTION'][$i]?></div>
			</div>			
		</div>
		<?
		$i++;
		endforeach;?>
	</div>
	<div class="slider">
		<div id="slider">
			<ul>
				<?
				$d=0;
				foreach($arResult['PROPERTIES']['GALLERY']['VALUE'] as $picId):
				$d++;
				$arThumb=CFile::ResizeImageGet($picId,Array("width"=>140,"height"=>140),BX_RESIZE_IMAGE_PROPORTIONAL,false);?>
				<li<?if($d==1):?> class="active"<?endif;?>><a href="#"><img src="<?=$arThumb['src']?>" alt="" /></a></li>
				<?endforeach;?>
			</ul>
		</div>
	</div>
</div>
<?endif;?>
<?endif;?>

<?=$arResult['DETAIL_TEXT']?>


<?if($arResult['PROPERTIES']['POSITION']['VALUE']=='Низ'):?>
<?if(count($arResult['PROPERTIES']['GALLERY']['VALUE'])>0 AND is_array($arResult['PROPERTIES']['GALLERY']['VALUE'])):?>
<div class="slider-block">
	<div class="big-pic">
		<?
		$i=0;
		foreach($arResult['PROPERTIES']['GALLERY']['VALUE'] as $picId):
		$arBigPic=CFile::ResizeImageGet($picId,Array("width"=>620,"height"=>500),BX_RESIZE_IMAGE_PROPORTIONAL,true);
		$arBig=CFile::GetFileArray($picId);
		?>
		<div class="item pic-<?=$i?>">
			<div class="over" style="width:<?=$arBigPic['width']?>px;">
				<a href="<?=$arBig['SRC']?>" class="fancybox" title="<?=$arResult['PROPERTIES']['GALLERY']['DESCRIPTION'][$i]?>" rel="useful-gallery"><img src="<?=$arBigPic['src']?>" alt="" /></a>
				<div class="name"><?=$arResult['PROPERTIES']['GALLERY']['DESCRIPTION'][$i]?></div>
			</div>			
		</div>
		<?
		$i++;
		endforeach;?>
	</div>
	<div class="slider">
		<div id="slider">
			<ul>
				<?
				$d=0;
				foreach($arResult['PROPERTIES']['GALLERY']['VALUE'] as $picId):
				$d++;
				$arThumb=CFile::ResizeImageGet($picId,Array("width"=>140,"height"=>140),BX_RESIZE_IMAGE_PROPORTIONAL,false);?>
				<li<?if($d==1):?> class="active"<?endif;?>><a href="#"><img src="<?=$arThumb['src']?>" alt="" /></a></li>
				<?endforeach;?>
			</ul>
		</div>
	</div>
</div>
<?endif;?>
<?endif;?>