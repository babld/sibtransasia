<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?if(count($arResult["SEARCH"])>0):?>
<div id="articles_listing">
	<?
	CModule::IncludeModule("iblock");
	foreach($arResult["SEARCH"] as $arItem):
		$res=CIBlockElement::GetByID($arItem['ITEM_ID']);
		$ar_res=$res->GetNext();
		$arItem['PREVIEW_PICTURE']=CFile::GetFileArray($ar_res['PREVIEW_PICTURE']);
	?>
	<div class="article_listing box-shadow">
		<h2><a href="<?=$arItem['URL']?>"><?=$arItem['TITLE']?></a></h2>
		<p>
			<?if($arItem['PREVIEW_PICTURE']['SRC']):?><img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="" /><?endif;?><?=$ar_res['PREVIEW_TEXT']?>
		</p>
		<?
		$arTags=explode(",",$arItem['~TAGS']);
		?>
		<div class="tags">
			<span>Теги:</span>
			<?
			$i=0;
			$cnt=count($arTags);
			foreach($arTags as $tag):
			$i++;
			?>
			<a href="/useful/tags/?q=<?=trim($tag);?>"><?=trim($tag);?></a><?if($cnt!=$i):?>, <?endif;?>
			<?endforeach;?>
		</div>
	</div>
	<?endforeach;?>
</div>
<?endif;?>