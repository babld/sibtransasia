<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="footer">
	<?$file_button = "Добавить файл";?>
	<div class="footer-container">
		<div class="footer-feedback">
			<div class="footer-feedback-manager">
				<div class="footer-feedback-manager-head">
					<div class="footer-manager-title">Суслов Иван</div>
					<div class="footer-manager-descr">Менеджер отдела ВЭД</div>
				</div>
				<div class="footer-manager-img">
					<img src="<?=SITE_TEMPLATE_PATH . "/images/footer-manager.jpg"?>"/>
				</div>
			</div>
			<form class="footer-feedback-form">
				<div class="footer-feedback-fields">
					<div class="footer-fields-desc">
						Оставьте свои контактные данные, и <b>мы расчитаем Вашу поставку</b> в течении 1 рабочего дня
					</div>
					<div class="footer-input-wrap">
						<input class="footer-input-field" type="text" placeholder="Ваше имя..." value="" name="name"/>
					</div>
					<div class="footer-input-wrap">
						<input class="footer-input-field" type="text" placeholder="Ваш телефон..." value="" name="phone"/>
					</div>
					<div class="footer-input-wrap">
						<input class="footer-input-field" type="text" placeholder="Ваш E-mail..." value="" name="email"/>
					</div>
				</div>
				<div class="footer-feedback-message">
					<div class="footer-message-desc">
						Введите описание груза, комментарий к поставке и код ТНВЭД, если известен:
					</div>
					<div class="footer-input-wrap">
						<textarea class="footer-input-field footer-message" name="message" placeholder="Описание груза, комментарий, код ТНВЭД..."></textarea>
					</div>
				</div>
				<div class="footer-feedback-buttons">
					<div class="upload_files">
						<div class="file_upload">
							<div class="footer-feedback-buttons-title">Файл не выбран</div>
							<button type="button" class="attach-file"><?=$file_button?></button>
							<input type="file" name="filename[]"/>
						</div>
					</div>
					<input class="footer-feedback-submit button-style" type="submit" value="Задать вопрос"/>
				</div>
			</form>
			<script>
				$(".footer-feedback-form").staFeedback();
				
				(function( $ ){
					$.fn.addAttachButton = function(options) {
						var settings = $.extend({
							"unbind"	: false
						}, options);
						
						var $this = $(this);
						
						var wrapper = $(this),
							inp = wrapper.find( "input" ),
							btn = wrapper.find( "button" ),
							lbl = wrapper.find( "div" );
							//del = wrapper.find(".delete_attach_file");
							
						btn.unbind();
						inp.unbind();
						/*del.unbind();
						
						del.click(function(){
							$this.remove();
						});*/
						
						// Crutches for the :focus style:
						btn.focus(function(){
							wrapper.addClass( "focus" );
						}).blur(function(){
							wrapper.removeClass( "focus" );
						});
					
						// Yep, it works!
						btn.add(lbl).click(function(){
						//btn.click(function(){
							inp.click();
						});
					
						var file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;
					
						inp.change(function(){
							var file_name;
							if( file_api && inp[0].files[0] )
								file_name = inp[0].files[0].name;
							else
								file_name = inp.val().replace( "C:\\fakepath\\", '' );
							if( ! file_name.length )
								return;
					
							if(lbl.is(":visible")){
								lbl.text(file_name);
								btn.text("<?=$file_button?>");
							}else
								btn.text(file_name);
						}).change();
					}
				})(jQuery);
				
				$(".file_upload").addAttachButton();
				
				
				$(window).resize(function(){
					$( ".file_upload input" ).triggerHandler("<?=$file_button?>");
				});
			</script>
		</div>
		<div class="footer-lead-status">
			<div class="footer-lead-title">Узнайте статус Вашей заявки</div>
			<div class="footer-lead-wrap">
				<form>
					<input type="text" class="footer-lead-input" name="leadid" placeholder="Введите номер заявки"/>
					<input type="submit" class="footer-lead-submit" value=""/>
					<textarea class="footer-input-status" disabled="disabled"/></textarea>
				</form>
			</div>
		</div>
		<div class="footer-menu">
			<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
				"ROOT_MENU_TYPE" => "bottom",
				"MENU_CACHE_TYPE" => "A",
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"MENU_CACHE_GET_VARS" => array(),
				"MAX_LEVEL" => "1",
				"CHILD_MENU_TYPE" => "",
				"USE_EXT" => "N",
				"DELAY" => "N",
				"ALLOW_MULTI_SELECT" => "N"
				),
				false
			);?>
		</div>

		<div class="footer-info-left">
			<div class="footer-slogan">Доверяйте работу профессионалам!</div>
			<div class="footer-disclaimer">Информация, представленная на <br/>сайте, не является публичной офертой</div>
		</div>
		<div class="footer-info-right">
			
			<div class="footer-icons-block">
				<a class="footer-mobile" href="http://m.sibtransasia.ru">Мобильная версия</a>
				<ul class="footer-icons">
					<li>
						<a target="_blank" class="footer-icon-m" href="http://m.sibtransasia.ru"></a>
					</li>
					<li>
						<a target="_blank" class="footer-icon-vk" href="http://vk.com/sibtransasia"></a>
					</li>
					<li>
						<a target="_blank" class="footer-icon-fb" href="https://www.facebook.com/sibtransasia"></a>
					</li>
					<li>
						<a target="_blank" class="footer-icon-tw" href="https://twitter.com/sibtransasia"></a>
					</li>
				</ul>
			</div>
			<div class="footer-copyright">2008 - 2015 &copy; sibtransasia.ru</div>
		</div>
	</div>
</div>
</body>
</html>