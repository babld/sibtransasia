<div class="header">
	<div id="fixed-header">
		<div class="layout-width">
			<div id="logo">
				<?if($dir[1]!=''):?><a href="/"><?endif;?>
				<img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt="" />
				<?if($dir[1]!=''):?></a><?endif;?>
			</div>
			<div class="header-title">Доставка и таможенное оформление товаров из Китая</div>
			<div class="header-contacts">
				<div class="header-contact">
					<div class="contact-notice"><a href="tel:8-800-775-67-58">звонок бесплатный</a></div>
					<div class="contact-val"><a href="tel:8-800-775-67-58">8-800-775-67-58</a></div>
				</div>
				<div class="header-contact">
					<div class="contact-notice"><a href="tel:+73832078860">Новосибирск</a></div>
					<div class="contact-val"><a href="tel:+73832078860">8-383-207-88-60</a></div>
				</div>
				<div class="header-contact">
					<div class="contact-notice email"><a href="mailto:<?=$kodermail?>"><?=$kodermail?></a></div>
					<div class="contact-val">E-mail:</div>
				</div>
				<form class="header-feedback">
					<input class="feedback-phone" type="text" name="phone" placeholder="Ваш телефон">
					<input class="feedback-submit button-style" type="submit" value="Перезвоните мне">
				</form>
			</div>
		</div>
	</div>
	<div class="top-menu">
		<div class="top-menu-content">
			<div class="layout-width">
				<?$APPLICATION->IncludeComponent("bitrix:menu", "top", array(
					"ROOT_MENU_TYPE" => "top",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"MENU_CACHE_GET_VARS" => array(),
					"MAX_LEVEL" => "1",
					"CHILD_MENU_TYPE" => "",
					"USE_EXT" => "N",
					"DELAY" => "N",
					"ALLOW_MULTI_SELECT" => "N"
					),
					false
				);?>
			</div>
		</div>
		<div class="shadow-bottom-block"></div>
	</div>
</div>