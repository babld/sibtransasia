<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?$dir=explode('/', $APPLICATION->GetCurDir());?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?$APPLICATION->ShowTitle()?></title>
	<meta http-equiv="content-language" content="ru" />
	<?$APPLICATION->ShowHead();?>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.1.9.1.min.js" type="text/JavaScript"></script>
	<!--script src="<?=SITE_TEMPLATE_PATH?>/js/easySlider1.7.js" type="text/JavaScript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/fancybox/jquery.fancybox.pack.js" type="text/JavaScript"></script-->
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.validate.min.js" type="text/JavaScript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.staFeedback.js" type="text/JavaScript"></script>
	<!--script src="<?=SITE_TEMPLATE_PATH?>/js/scripts.js" type="text/JavaScript"></script-->
	<script src="<?=SITE_TEMPLATE_PATH?>/js/common.js" type="text/JavaScript"></script>
	<!--script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.carouFredSel-6.2.1-packed.js" type="text/JavaScript"></script-->

	<!--script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.maskedinput.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/additional-methods.js" type="text/javascript"></script-->

	<!--link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/fancybox/jquery.fancybox.css" /-->
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/common.css" />
	<!--link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/yandex-search.css" />
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/china-search.css" /--><?
	switch($dir[1]):
		case "about":
			$class="inner_page text_page";
		break;
		case "useful":
			$class="inner_page";
		break;
		case "goods":
			$class="goods";
		break;
		case "prices":
			$class="inner_page prices";
		break;
		case "contacts":
			$class="contacts";
		break;
		case "vacancies":
			$class="inner_page vacancies";
		break;
		default:
			echo '<link rel="stylesheet" type="text/css" href="' . SITE_TEMPLATE_PATH . '/css/index.css" />';
			echo '<script src="' . SITE_TEMPLATE_PATH . '/js/index.js" type="text/JavaScript"></script>';
			break;
	endswitch;?>

	<!--[if lte IE 8]>
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/ie8.css" />
	<![endif]-->
	
	<?$cur_page = $APPLICATION->GetCurPage(true);?>
	<meta name='yandex-verification' content='41c13da7d6cf089f' /><?
	$APPLICATION->SetPageProperty("description", "Мы реализуем услугу полностью, без привлечения посредников. Это помогает избежать ошибок в работе и ускорить доставку. На нашем рынке этот подход важен вдвойне. Только лучшие компании могут решить задачу без привлечения третьих лиц 8-800-555-9766");
	$APPLICATION->SetPageProperty("keywords", "сибирьтрансазия sibtransasia доставка товаров китая россию сибирь транс азия оборудование цены доставки карго грузов казахстана компания импорт бизнес новосибирск");
	$APPLICATION->SetPageProperty("title", "Доставка грузов из Китая через Казахстан | Таможенное оформление | Складская логистика");
	$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
	$APPLICATION->SetTitle("Главная страница");?>
</head>
<body class="<?=$class?>">
<?if($USER->IsAuthorized()):?>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<?endif;
$Mail = "info@sibtransasia.ru";

$j = 0;
$kodermail = '';
for($j = 0; $j < strlen($Mail); $j++) {
	$kodermail .= '&#' . ord(substr($Mail, $j, 1)) . ';';
}?>

