<table id="regions" class="representation w-clear" cellspacing="0">
	<?
	CModule::IncludeModule('iblock');
	$arSelect=Array("NAME","PROPERTY_TYPE","CODE");
	$arFilter=Array("IBLOCK_ID"=>8,"ACTIVE"=>"Y");
	$dbRes=CIBlockElement::GetList(Array("SORT"=>"ASC"),$arFilter,false,false,$arSelect);
	$i=0;
	while($arRes=$dbRes->GetNext()):
        $i++;
		if($i % 4 == 1)
			echo("<tr>"); ?>
        <td class="agency">
            <a href="#<?=$arRes['CODE']?>">
                <span class="title"><?=$arRes['NAME']?></span>
                <?=$arRes['PROPERTY_TYPE_VALUE']?>
            </a>
        </td>
		<?if($i % 4 == 0 and $i > 1)
			echo("</tr>");?>
	<?endwhile;?>
</table>