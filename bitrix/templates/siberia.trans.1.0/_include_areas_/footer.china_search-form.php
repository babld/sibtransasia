<?php
	$file_button = "Добавить файлы";
?>
<div class="china_search-form-wrap box-shadow">
	<form id="china_search-form" method="POST" enctype="multipart/form-data" action="/services/emailsend.php" onsubmit="yaCounter23212990.reachGoal('china_search_footer'); yaCounter23212990.reachGoal('all');return true;">
		<input type="hidden" name="subject" value="с поиска по Китаю"/>
		<div class="form-left">
			<div class="form-title">Нужно найти товар в Китае и привезти в Россию?</div>
			<div class="proem">
				<div class="proem-text">Отправьте запрос на поиск специалистам.</div>
				<div class="proem-losung">Доверьте работу профессионалам!</div>
			</div>
			<div class="idents">
				<div class="inpt">
					<input type="text" id="fName" name="name" placeholder="Имя" />
				</div>
				<div class="inpt">
					<input type="text" id="fPhone" name="phone" placeholder="Телефон" class="fPhone"/>
				</div>
				<div class="inpt last">
					<input type="text" id="fMail" name="email" class="fMail" placeholder="Эл. почта" />
				</div>
			</div>
		</div>
		
		<div class="message">
			<textarea name="message" placeholder="Введите товарную группу, описание товара, ссылку на необходимый товар и параметры поставки" class="fMsg"></textarea>
		</div>
		
		<div class="buttons">
			<div class="upload_files">
				<div class="file_upload">
					<div>Файл не выбран</div>
					<button type="button" class="as-button white"><?=$file_button?></button>
					<input type="file" name="filename[]"/>
				</div>
			</div>
			<a class="buttons__more-files" href="javascript:void(0)">Прикрепить еще</a>
			<div class="submit box-shadow">
				<input type="submit" name="submit" value="Найти и доставить" />
			</div>
		</div>
		
		<div class="clear"></div>
	</form>
	<script>
		$("#china_search-form").staFeedback({"submitHandler":false});
			
		(function( $ ){
			$.fn.addAttachButton = function(options) {
				var settings = $.extend({
					"unbind"	: false
				}, options);
				
				var $this = $(this);
				
				var wrapper = $(this),
					inp = wrapper.find( "input" ),
					btn = wrapper.find( "button" ),
					lbl = wrapper.find( "div" ),
					del = wrapper.find(".delete_attach_file");
					
				btn.unbind();
				inp.unbind();
				del.unbind();
				
				del.click(function(){
					$this.remove();
				});
				
				// Crutches for the :focus style:
				btn.focus(function(){
					wrapper.addClass( "focus" );
				}).blur(function(){
					wrapper.removeClass( "focus" );
				});
			
				// Yep, it works!
				btn.add(lbl).click(function(){
				//btn.click(function(){
					inp.click();
				});
			
				var file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;
			
				inp.change(function(){
					var file_name;
					if( file_api && inp[0].files[0] )
						file_name = inp[0].files[0].name;
					else
						file_name = inp.val().replace( "C:\\fakepath\\", '' );
					if( ! file_name.length )
						return;
			
					if(lbl.is(":visible")){
						lbl.text(file_name);
						btn.text("<?=$file_button?>");
					}else
						btn.text(file_name);
				}).change();
			}
		})(jQuery);
		
		$(".file_upload").addAttachButton();
		
		$(".buttons__more-files").on("click", function(){
			$(".upload_files").append('<div class="file_upload">\
					<div>Файл не выбран</div>\
					<button type="button" class="as-button white"><?=$file_button?></button>\
					<input type="file" name="filename[]"/>\
					<i class="delete_attach_file"></i>\
				</div>');
			$(".file_upload").each(function(){
				$(this).addAttachButton();
			});
		});
		
		$(window).resize(function(){
			$( ".file_upload input" ).triggerHandler("<?=$file_button?>");
		});
	</script>
</div>

<div class="clear"></div>