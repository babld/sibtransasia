<noindex>
    <div class="footer-social-buttons">
        <a target="_blank" rel="nofollow" href="javascript:void(0)" onclick="window.open('https://www.facebook.com/sibtransasia', '_blank')">
            <img title="СибирьТрансАзия в facebook" alt="СибирьТрансАзия в facebook" src="<?=SITE_TEMPLATE_PATH?>/images/social-buttons/fb.png"/>
        </a>
        <a target="_blank" rel="nofollow" href="javascript:void(0)" onclick="window.open('http://vk.com/sibtransasia', '_blank')">
            <img title="СибирьТрансАзия вконтакте" alt="СибирьТрансАзия вконтакте" src="<?=SITE_TEMPLATE_PATH?>/images/social-buttons/vk.png"/>
        </a>
        <a target="_blank" rel="nofollow" href="javascript:void(0)" onclick="window.open('https://twitter.com/sibtransasia', '_blank')">
            <img title="СибирьТрансАзия в твиттере" alt="СибирьТрансАзия в твиттере" src="<?=SITE_TEMPLATE_PATH?>/images/social-buttons/tw.png"/>
        </a>
    </div>
</noindex>