<h2>Наши вакансии</h2>
<ul id="regions" class="menuSideSelector">
	<?
	CModule::IncludeModule('iblock');
	$arSelect=Array("NAME","PROPERTY_TYPE","CODE");
	$arFilter=Array("IBLOCK_ID"=>11,"ACTIVE"=>"Y");
	$dbRes=CIBlockElement::GetList(Array("SORT"=>"ASC"),$arFilter,false,false,$arSelect);
	$i=0;
	while($arRes=$dbRes->GetNext()):
	$i++;
	?>
	<li>
		<a <?if($i==1):?>class="active"<?endif;?> href="#<?=$arRes['CODE']?>">
			<span class="title"><?=$arRes['NAME']?></span>
			<?=$arRes['PROPERTY_TYPE_VALUE']?>
		</a>
	</li>
	<?endwhile;?>
</ul>