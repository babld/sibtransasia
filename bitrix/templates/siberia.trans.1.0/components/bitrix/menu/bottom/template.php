<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<div class="navigation">
<?
$cnt=count($arResult);
foreach($arResult as $arItem):
	$i++; ?>	
	<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a> <?if($cnt!=$i):?>|<?endif;?>
<?endforeach?>
</div>
<?endif?>