<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<h2>Популярные вопросы</h2>		
<div id="faq">
	<ul>
		<?foreach($arResult["ITEMS"] as $arItem):?>
		<li><a rel="gallery" class="fancybox fancybox.ajax" href="/about/faq/ajax-detail.php?id=<?=$arItem['ID']?>"><?=$arItem['NAME']?></a></li>
		<?endforeach;?>
	</ul>		
	<a id="all_questions" href="/about/faq/">Все вопросы</a>
	<div class="image"></div>
</div>