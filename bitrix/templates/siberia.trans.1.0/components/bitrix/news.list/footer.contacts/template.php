<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="contact">
	<div class="image-carousel-wrapper">
		<div class="image-carousel">
			<?foreach($arResult["ITEMS"] as $arItem):
				$Mail = $arItem['PROPERTIES']['EMAIL']['VALUE'];
				
				$j = 0;
				$kodermail = '';
				for($j = 0; $j < strlen($Mail); $j++) {
					$kodermail .= '&#' . ord(substr($Mail, $j, 1)) . ';';
				}
				?>
				<div class="carousel-item">
					<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>" />
					<ul class="contacts-detail">
						<li class="name">
							<?=$arItem['NAME']?>
						</li>
						<li>
							<span class="phone">
								<?=$arItem['PROPERTIES']['PHONE']['VALUE']?>
							</span>
						</li>
						<? if(!empty($arItem['PROPERTIES']['MOBILE']['VALUE'])) : ?>
							<li>
								<span class="mobile">
									<?=$arItem['PROPERTIES']['MOBILE']['VALUE']?>
								</span>
							</li>
						<? endif; ?>
						<li>
							<span class="mail">
								<a href="mailto:<?=$kodermail?>">
									<?=$kodermail?>
								</a>
							</span>
						</li>
						<li>
							<span class="skype">
								<a href="callto:<?=$arItem['PROPERTIES']['SKYPE']['VALUE']?>">
									<?=$arItem['PROPERTIES']['SKYPE']['VALUE']?>
								</a>
							</span>
						</li>
					</ul>
					<div class="clear"></div>
				</div>
			<?endforeach;?>
		</div>
		<div class="clearfix"></div>
		<a class="prev" id="image-carousel-prev" href="#"><span>prev</span></a>
		<a class="next" id="image-carousel-next" href="#"><span>next</span></a>
		<div class="pagination" id="foo2_pag"></div>
	</div>
	<script>
		$(".image-carousel").carouFredSel({
			infinite: false,
			//circular: false,
			auto	:	false,
			prev	:	{
				button	: "#image-carousel-prev",
				key		: "left"
			},
			next	: {
				button	: "#image-carousel-next",
				key		: "right"
			},
			pagination	: "#foo2_pag"
		});
	</script>
</div>