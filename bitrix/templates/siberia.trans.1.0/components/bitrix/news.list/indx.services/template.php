<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="services">		
	<ul>
<?
$i=0;
foreach($arResult["ITEMS"] as $arItem):
$i++;
if($i==1):
	$Text=$arItem['PREVIEW_TEXT'];
endif;?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<li class="i-<?=$i?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<a href="<?=$arItem['PROPERTIES']['URL']['VALUE']?>">
			<span class="ico"></span>
			<span class="desc"><span><?=$arItem['NAME']?></span></span>
			<span class="description"><?=$arItem['PREVIEW_TEXT']?></span>
		</a>
	</li>
<?endforeach;?>
	</ul>
	<div class="clear"></div>
</div>
<em id="services_description" class="box-shadow"><span><?=$Text?></span></em>