<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="slider">
	<ul>
	<?foreach($arResult["ITEMS"] as $arItem):?>
	<li>
		<?if($arItem['PROPERTIES']['URL']['VALUE']):?><a href="<?=$arItem['PROPERTIES']['URL']['VALUE']?>"><?endif;?>
		<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="" />
		<?if($arItem['PROPERTIES']['URL']['VALUE']):?></a><?endif;?>
	</li>
	<?endforeach;?>
	</ul>
	<div class="clear"></div>
</div>