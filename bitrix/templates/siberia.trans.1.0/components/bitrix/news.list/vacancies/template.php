<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<ul id="offices" >
<?
$i=0;
foreach($arResult["ITEMS"] as $arItem):
	$i++;?>

	<li id="<?=$arItem['CODE']?>" <?if($i==1):?>class="current<?endif;?>">
		<div class="info">
			<h2><?=$arItem['NAME']?></h2>
			<?=$arItem['DETAIL_TEXT']?>
		</div>
	</li>
<?endforeach;?>
</ul>
