<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="articles_listing">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="article_listing box-shadow">
		<h2><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a></h2>

        <p>
            <?if($arItem['PREVIEW_PICTURE']['SRC']):?>
                <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="" />
            <?endif;?>
            <?=$arItem['PREVIEW_TEXT']?>
        </p>

		<?
		$arTags=explode(",",$arItem['TAGS']);
		?>
		<div class="tags">
			<span>Теги:</span>
			<?
			$i=0;
			$cnt=count($arTags);
			foreach($arTags as $tag):
			$i++;
			?>
			<a href="tags/?q=<?=trim($tag);?>"><?=trim($tag);?></a><?if($cnt!=$i):?>, <?endif;?>
			<?endforeach;?>
		</div>
	</div>
<?endforeach;?>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><?=$arResult["NAV_STRING"]?><?endif;?>