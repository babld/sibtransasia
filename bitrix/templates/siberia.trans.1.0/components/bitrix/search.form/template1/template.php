<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="search-form">
	<form action="<?=$arResult["FORM_ACTION"]?>">
		<fieldset>
			<?php if(isset($_GET['q']) and !empty($_GET['q'])) : ?>
				<input class="search-form__query" type="text" name="q" value="<?=$_GET['q']?>" >
			<?php else : ?>
				<input class="search-form__query" type="text" name="q" placeholder="Поиск.." >
			<?php endif ?>

			<input class="search-form__submit" name="s" type="submit" value="<?#=GetMessage("BSF_T_SEARCH_BUTTON");?>" />
		</fieldset>
	</form>
</div>