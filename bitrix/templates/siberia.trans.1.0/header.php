<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?$APPLICATION->ShowTitle()?></title>
	<meta http-equiv="content-language" content="ru" />
	<?$APPLICATION->ShowHead();?>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.1.9.1.min.js" type="text/JavaScript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/easySlider1.7.js" type="text/JavaScript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/fancybox/jquery.fancybox.pack.js" type="text/JavaScript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.validate.min.js" type="text/JavaScript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.staFeedback.js" type="text/JavaScript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/scripts.js" type="text/JavaScript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.carouFredSel-6.2.1-packed.js" type="text/JavaScript"></script>

	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.maskedinput.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/additional-methods.js" type="text/javascript"></script>

	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/fancybox/jquery.fancybox.css" />
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/yandex-search.css" />
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/china-search.css" />
	<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
	<!--[if lte IE 8]>
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/ie8.css" />
	<![endif]-->
	<?$dir=explode('/', $APPLICATION->GetCurDir());?>
	<?$cur_page = $APPLICATION->GetCurPage(true);?>
	<meta name='yandex-verification' content='41c13da7d6cf089f' />

	<!-- Google Analythics -->
	<script type="text/javascript">
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-41415433-1', 'sibtransasia.ru');
		ga('send', 'pageview');
	</script>
	<!-- Google Analythics -->
</head>
<body>
<?if($USER->IsAuthorized()):?>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<?endif;
$Mail = "info@sibtransasia.ru";

$j = 0;
$kodermail = '';
for($j = 0; $j < strlen($Mail); $j++) {
	$kodermail .= '&#' . ord(substr($Mail, $j, 1)) . ';';
}
				?>
<div class="alignment">
	<div id="header">
		<div id="logo">
			<?if($dir[1]!=''):?><a href="/"><?endif;?>
			<img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt="" />
			<?if($dir[1]!=''):?></a><?endif;?>
		</div>
		<a class="header-mail" href="mailto:<?=$kodermail?>"><?=$kodermail?></a>
		
			
		<div id="phone_block">
			<?$APPLICATION->IncludeFile(
				SITE_TEMPLATE_PATH.'/_include_areas_/header.phone.php',
				array(),
				array('MODE'=>'text','SHOW_BORDER'=>true)
			);?>
		</div>
		<div class="call-buttons">
			<?$APPLICATION->IncludeFile(
				SITE_TEMPLATE_PATH.'/_include_areas_/header.call.php',
				array(),
				array('MODE'=>'html', 'SHOW_BORDER'=>true)
			);?>
		</div>
		
		<div class="header-search-block">
			<?$APPLICATION->IncludeFile(
				SITE_TEMPLATE_PATH.'/_include_areas_/header.search.php',
				array("dir"=>$dir[1]),
				array('MODE'=>'html', 'SHOW_BORDER'=>true)
			);?>
		</div>
		
		<?$APPLICATION->IncludeComponent("bitrix:menu", "top", array(
			"ROOT_MENU_TYPE" => "top",
			"MENU_CACHE_TYPE" => "A",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => array(),
			"MAX_LEVEL" => "1",
			"CHILD_MENU_TYPE" => "",
			"USE_EXT" => "N",
			"DELAY" => "N",
			"ALLOW_MULTI_SELECT" => "N"
			),
			false
		);?>
		<div class="clear"></div>
		<!--?$APPLICATION->IncludeFile(
			SITE_TEMPLATE_PATH.'/_include_areas_/header.presentation.php',
			array(),
			array('MODE'=>'html','SHOW_BORDER'=>true)
		);?-->
		<div class="special-offer box-shadow">
			<a class="box-shadow" href="/prices/#full-truck-consignment"><span class="red">Специальное предложение!</span> 105 кубов / 20 тонн из Китая в <span class="special-whole-city"></span> за <span class="red special-whole-day"></span> и <span class="red special-whole-price"></span>!</a>
		</div>


	</div>

		<?if($dir[1]=='about'):?>
			<div id="sidebar">
				<?$APPLICATION->IncludeFile(
					SITE_TEMPLATE_PATH.'/_include_areas_/about.side.php',
					array(),
					array('MODE'=>'php','SHOW_BORDER'=>false)
				);?>
			</div>
		<?elseif($dir[1]=='useful'):?>
			<div id="sidebar">
				<?$APPLICATION->IncludeFile(
					SITE_TEMPLATE_PATH.'/_include_areas_/useful.side.php',
					array(),
					array('MODE'=>'php','SHOW_BORDER'=>false)
				);?>
			</div>
		<?elseif($dir[1]=='goods'):?>
			<div id="sidebar">
				<?$APPLICATION->IncludeFile(
					SITE_TEMPLATE_PATH.'/_include_areas_/goods.side.php',
					array(),
					array('MODE'=>'php','SHOW_BORDER'=>false)
				);?>
			</div>
		<?elseif($dir[1]=='prices'):?>
			<div id="sidebar">
				<?$APPLICATION->IncludeFile(
					SITE_TEMPLATE_PATH.'/_include_areas_/prices.side.php',
					array(),
					array('MODE'=>'php','SHOW_BORDER'=>false)
				);?>
			</div>
		<?elseif($dir[1]=='vacancies'):?>
			<div id="sidebar">
				<?$APPLICATION->IncludeFile(
				SITE_TEMPLATE_PATH.'/_include_areas_/vacancies.side.php',
				array(),
				array('MODE'=>'php','SHOW_BORDER'=>false)
			);?>
			</div>
		<?endif;?>
	<?
	switch($dir[1]):
		case "about":
			$class="inner_page text_page";
		break;
		case "useful":
			$class="inner_page";
		break;
		case "goods":
			$class="inner_page";
		break;
		case "prices":
			$class="inner_page prices";
		break;
		case "contacts":
			$class="contacts";
		break;
		case "vacancies":
			$class="inner_page vacancies";
		break;
		default:
			$class="index_page";
		break;
	endswitch;?>
	
	<div id="content" class="<?=$class?>">	