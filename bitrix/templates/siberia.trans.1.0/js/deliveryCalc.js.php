<script type="text/javascript">
	form			= $("#ajax-delivery-calc-form");
	formFeedback	= $(".delivery-calc__feedback-form"); <?
	
	$url	= "http://www.cbr.ru/scripts/XML_daily.asp?date_req=" . date("d.m.Y");
	
	if($url) {
		if($XML	= simplexml_load_file($url)) {
			$yuan	= $XML->xpath('/ValCurs/Valute[@ID="R01375"]/Value');
			$usd 	= $XML->xpath('/ValCurs/Valute[@ID="R01235"]/Value');
		}
	}
	$yuan_nominal = $XML->xpath('/ValCurs/Valute[@ID="R01375"]/Nominal');
	$yuan_nominal = $yuan_nominal[0];?>
	
	yuan	= (<?=((float)str_replace(',', '.', $yuan[0])/(int)$yuan_nominal)?>).toFixed(2);
	usd		= (<?=((float)str_replace(',', '.', $usd[0]))?>).toFixed(2);
	
	$(".delivery-calc__usd-rate").html(usd);
	$(".delivery-calc__yuan-rate").html(yuan);

	function removeFiles() {
		$(".delivery-calc__file i").unbind('click');
		$(".delivery-calc__file i").bind('click', function(){
			$(this).parent().remove();
		});
	}

	$(".delivery-calc__moreFiles").on('click', function(){
		$(".delivery-calc__files").append('<div class="delivery-calc__file"><i></i><input name="filename[]" type="file"/></div>');
		removeFiles()
	});
	
	var min_weight = 100;
	
	form.validate({
		rules: {
			city	: {
				required: true
			},
			weight	: {
				required	: true,
				number		: true,
				max			: 20000,
				min			: min_weight
			},
			volume	: {
				required	: true,
				number		: true,
				max			: 110,
				min			: 1
			}
		},
		messages: {
			city	: {
				required	: "Выберите, пожалуйста, город",
			},
			weight	: {
				required	: "Укажите, пожалуйста, вес",
				number		: "Введите числовое значение",
				max			: "Вес должен быть не более 20 т",
				min			: "Вес должен быть больше " + min_weight +" кг"
			},
			volume	: {
				required	: "Укажите, пожалуйста, объем",
				number		: "Введите числовое значение",
				max			: "Объем не более 110 м3",
				min			: "Объем должен быть больше 1 м3"
			}
		},
		submitHandler: submit
	});
	
	function submit(formData) {
		
		<?$APPLICATION->IncludeFile(
			SITE_TEMPLATE_PATH.'/_include_areas_/calc.parameters.php',
			array(),
			array('MODE'=>'php','SHOW_BORDER'=>false)
		);?>
	
		var weight 			= formData.weight.value;
		var volume 			= formData.volume.value;
		var city 			= formData.city.value;

	
		// Переменные из select - а
		var price_lk_l20m3 	= Number($("form :selected").attr("data-price_lk_l20m3"));
		var price_lk_g20m3 	= Number($("form :selected").attr("data-price_lk_g20m3"));
		var price_hk_l20t 	= Number($("form :selected").attr("data-price_hk_l20t"));
		var price_hk_g20t 	= Number($("form :selected").attr("data-price_hk_g20t"));
		var price_wp 		= Number($("form :selected").attr("data-price_wp"));

		for(var i = 0; i< formData.contract.length; i++) {
			if (formData.contract[i].checked) {
				contract = formData.contract[i].value;
			}
		}
		
		var density 		= weight / volume;
		
		var isLightCube 	= density > 220 ? false : true;
		var isWholeParty	= false;
		var rate 			= $(".delivery-calc__rate");
		var cargoType		= $(".delivery-calc__cargoType");
	
		var total_nsk, total_china, totalRub, totalUSD;

		if (isLightCube) {
	
			//Расчет для доставки по Китаю
			if (volume < 20) {
				total_china = volume * price_lk_l20m3;
			} else {
				total_china = volume * price_lk_g20m3;
			}

			//Расчет доставки от Урумчи до Новосибирска
			if(volume > 0 && volume <= 25) {
				total_nsk = urumchiNskDelivery[contract][0] * volume;
			} else if(volume > 25 && volume <= 70) {
				total_nsk = urumchiNskDelivery[contract][1] * volume;
			} else if(volume > 70 && volume <= 110) {
				total_nsk = urumchiNskDelivery[contract][2] * volume;
			}

			if(volume >= 100) {
				isWholeParty = true;
			}

			rate.html(" кубический метр (м<sup>3</sup>) ");

		} else {
			// Расчет доставки по Китаю
			if (weight < 2000) {
				total_china = weight * price_hk_l20t;
			} else {
				total_china = weight * price_hk_g20t;
			}

			//Расчет доставки от Урумчи до Новосибирска
			if(weight >= min_weight && weight <= 5000) {
				total_nsk = weight * urumchiNskDelivery[contract][3];
			} else if(weight > 5000 && weight <= 14000) {
				total_nsk = weight * urumchiNskDelivery[contract][4];
			} else if(weight > 14000 && weight <= 20000) {
				total_nsk = weight * urumchiNskDelivery[contract][5];
			}
			
			if(weight > 20000) {
				isWholeParty = true;
			}
			
			rate.html(" киллограмм ");
		}
		
		//Если получившаяся цена больше чем цена за цельную партию принимаем что это цельная партия и ставим цену такую же как у цельной партии
		if(total_nsk > wholePartPrice[contract]) {
			total_nsk = Number(wholePartPrice[contract]);
			isWholeParty = true;
		}
		
		if(isWholeParty) {
			total_china = price_wp;
			total_nsk = wholePartPrice[contract];
			cargoType.html("цельной партии");
		} else {
			cargoType.html("сборного груза");
		}

		//console.log("isLightCube = " + isLightCube + " total_china = " + total_china + " yuan = " + yuan + " usd = " + usd + " total_nsk = " + total_nsk);
		
		
		totalUSD = (total_china * yuan / usd + total_nsk * 1).toFixed(2);
		totalRub = (total_china * yuan + total_nsk * usd).toFixed(2);

		$(".delivery-calc__total.totalUSD").val(totalUSD);
		$(".delivery-calc__total.totalRub").val(totalRub);
		$(".delivery-calc__senderCity").html($("form :selected").val());
		$(".delivery-calc__notice, #delivery-calc__feedback").show();

		$(".delivery-calc__senderCity").html(city);
		$(".delivery-calc__hiddenFrom").val(city);
		$(".delivery-calc__hiddenVolume").val(volume);
		$(".delivery-calc__hiddenWeight").val(weight);
		$(".delivery-calc__totalUSD").val(totalUSD);
		$(".delivery-calc__totalRUB").val(totalRub);
	
		$(".delivery-calc__notice").find(".lt-500").remove();
		
		if (weight < 500) {
			
			$(".delivery-calc__notice").append("<span class='lt-500'>При поставке общим весом менее 500 кг стоимость доставки рассчитывается индивидуально.</span>");
		}
		
		$(".delivery-calc__notice").val()
		
		
		if(contract == "our")
			contractM = "Наш";
		else if (contract == "your")
			contractM = "Контракт клиента";
		
		$(".delivery-calc__hiddenContract").val(contractM);
	}
	
	formFeedback.validate({
		rules: {
			name 			: {
				required	: true
			},

			phone			: {
				required	: true
			},

			email			: {
				required	: true,
				email		: true
			}
		},
		messages : {
			name		: {
				required	: "Введите имя"
			},
			phone 		: {
				required	: "Введите номер телефона"
			},
			email		: {
				required	: "Введите электронную почту",
				email		: "Введите корректно"
			}
		},
		errorPlacement: function(error, element) {},
		submitHandler: submitFeedback
	});
	
	function submitFeedback(form) {
		form.submit();
		//$(".delivery-calc__feedback-form").replaceWith("Ваша заявка принята. Мы свяжемся с Вами в течении часа!");
	}

	function parseResponce(response) {
		if (response) {
			formFeedback.replaceWith("<div class='thankyou-message'>Ваша заявка принята.<br/>Мы свяжемся с Вами в течении часа!</div>");
		}
	}
	
</script>
