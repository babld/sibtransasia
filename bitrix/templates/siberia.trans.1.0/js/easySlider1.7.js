﻿/*
 * 	Easy Slider - jQuery plugin
 *	written by Alen Grakalic	
 *	http://cssglobe.com/post/3783/jquery-plugin-easy-image-or-content-slider
 *
 *	Copyright (c) 2009 Alen Grakalic (http://cssglobe.com)
 *	Dual licensed under the MIT (MIT-LICENSE.txt)
 *	and GPL (GPL-LICENSE.txt) licenses.
 *
 *	Built for jQuery library
 *	http://jquery.com
 *
 */
 
/*
 *	markup example for $("#images").easySlider();
 *	
 * 	<div id="images">
 *		<ul>
 *			<li><img src="images/01.jpg" alt="" /></li>
 *			<li><img src="images/02.jpg" alt="" /></li>
 *			<li><img src="images/03.jpg" alt="" /></li>
 *			<li><img src="images/04.jpg" alt="" /></li>
 *			<li><img src="images/05.jpg" alt="" /></li>
 *		</ul>
 *	</div>
 *
 */

(function($) {

	$.fn.easySlider = function(options){
	  
		// default configuration properties
		var defaults = {
			prevId: 		'prevBtn',
			prevText: 		'Previous',
			nextId: 		'nextBtn',	
			nextText: 		'Next',
			orientation:	'', //  'vertical' is optional;
			speed: 			800,
			a:              4 //Р°РљР°РћР°Р›Р°РР±В‡Р°Р•Р±ВЃР±В‚Р°Р’Р°Рћ Р±ВЌР°Р›Р°Р•Р°РњР°Р•Р°РќР±В‚Р°РћР°Р’ li Р°Р’ Р°РћР°Р‘Р°Р›Р°РђР±ВЃР±В‚Р°Р Р°РћР±В‚Р°РћР°Р‘Р±ВЂР°РђР°Р–Р°Р•Р°РќР°РР±ВЏ
		}; 
		
		var options = $.extend(defaults, options);  
		
		return this.each(function() {  
			obj = $(this); 				
			var s = $("li", obj).length; //Р°РљР°РћР°Р›Р°РР±В‡Р°Р•Р±ВЃР±В‚Р°Р’Р°Рћ Р±ВЌР°Р›Р°Р•Р°РњР°Р•Р°РќР±В‚Р°РћР°Р’ li
			var w = $("li", obj).width(); //Р±В€Р°РР±ВЂР°РР°РќР°Рђ Р±ВЌР°Р›Р°Р•Р°РњР°Р•Р°РќР±В‚Р°Рђ li
			var h = $("li", obj).height(); //Р°Р’Р±В‹Р±ВЃР°РћР±В‚Р°Рђ Р±ВЌР°Р›Р°Р•Р°РњР°Р•Р°РќР±В‚Р°Рђ li
			
			//Р±В€Р°РР±ВЂР°РР°РќР°Рђ Р°РћР°Р‘Р°Р›Р°РђР±ВЃР±В‚Р°Р Р°РћР±В‚Р°РћР°Р‘Р±ВЂР°РђР°Р–Р°Р•Р°РќР°РР±ВЏ Р±ВЌР°Р›Р°Р•Р°РњР°Р•Р°РќР±В‚Р°РћР°Р’ li
			//Р°Рђ Р±В‚Р°РђР°РљР°Р–Р°Р• Р°РћР°Р‘Р±ВЂР°Р•Р°Р—Р°РђР°Р•Р°Рњ Р°Р’Р±ВЃР°Р• Р±В‡Р±В‚Р°Рћ Р°Р’Р±В‹Р±В…Р°РћР°Р”Р°РР±В‚ Р°Р—Р°Рђ Р°РџР±ВЂР°Р•Р°Р”Р°Р•Р°Р›Р±В‹ - 'overflow': 'hidden'
			obj.css({'width': w*options.a, 'height': h, 'overflow': 'hidden'});
			
			//Р°РџР±ВЂР°Р•Р°Р”Р°Р•Р°Р› Р°РќР°Рђ Р°РљР°РћР±В‚Р°РћР±ВЂР°РћР°Рњ Р°РџР°Р•Р±ВЂР°Р•Р±ВЃР±В‚Р°РђР°Р•Р°Рњ Р°Р›Р°РР±ВЃР±В‚Р°РђР±В‚Р±ВЊ next Р°РќР°РђР°РџР±ВЂ Р°Р•Р±ВЃР°Р›Р°Р s=16 Р°Р a=4 Р±В‚Р°Рћ 16-4=12, Р°РџР±ВЂР°РћР°Р›Р°РР±ВЃР±В‚Р°РђР°Р’ Р°Р”Р°Рћ 12`Р°Р“Р°Рћ Р±ВЌР°Р›-Р±В‚Р°Рђ
			//Р°РџР±ВЂР°Р•Р°РљР±ВЂР°РђР±В‰Р°РђР°Р•Р°Рњ Р°Р›Р°РР±ВЃР±В‚Р°РђР±В‚Р±ВЊ next
			var ts = s-options.a;
			
			var t = 0;
			var vertical = (options.orientation == 'vertical');
			$("ul", obj).css('width',s*w);			
			if(!vertical) $("li", obj).css('float','left');
			$(obj).after('<span id="'+ options.prevId +'"><a href=\"javascript:void(0);\">'+ options.prevText +'</a></span> <span id="'+ options.nextId +'"><a href=\"javascript:void(0);\">'+ options.nextText +'</a></span>');		
			$("a","#"+options.prevId).hide();
			$("a","#"+options.nextId).hide();
			$("a","#"+options.nextId).click(function(){		
				animate("next");
				if (t>=ts) $(this).fadeOut();
				$("a","#"+options.prevId).fadeIn();
			});
			$("a","#"+options.prevId).click(function(){		
				animate("prev");
				if (t<=0) $(this).fadeOut();
				$("a","#"+options.nextId).fadeIn();
			});	
			function animate(dir){
				if(dir == "next"){
					t = (t>=ts) ? ts : t+1;	
				} else {
					t = (t<=0) ? 0 : t-1;
				};								
				if(!vertical) {
					p = (t*w*-1);
					$("ul",obj).animate(
						{ marginLeft: p }, 
						options.speed
					);				
				} else {
					p = (t*h*-1);
					$("ul",obj).animate(
						{ marginTop: p }, 
						options.speed
					);					
				}
			};
			if(s>1) $("a","#"+options.nextId).fadeIn();	
		});
	  
	};

})(jQuery);
