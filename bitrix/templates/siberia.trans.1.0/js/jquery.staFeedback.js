(function( $ ){
    $.fn.staFeedback = function(options) {

        var settings = $.extend({
            'extendFormOn'  :       '.extendFormOn',
            'extendFormOff' :       '.extendFormOff',
            'extendForm'    :       '.extendForm',
            'extendOn'      :       '.extendOn',

            'fMsg'          :       '.fMsg',
            
            'extended'      :       false,
            'fPhone'        :       '.fPhone',
            'fMail'         :       '.fMail',
            
            'submitHandler' :       true
            
        
        }, options);

        var thisForm = this;

        this.find(settings.extendOn).click(function()
        {
            //console.log($(this));
            if($(this).parent().find(".extendForm").is(":visible")){
                $(this).parent().find("input[type='submit']").val('Заказать');
            } else {
                $(this).parent().find("input[type='submit']").val('Отправить');
            }
            
            $(this).find(settings.extendFormOff).toggle();
            $(this).find(settings.extendFormOn).toggle();
            thisForm.find(settings.extendForm).toggle();
            return false;
        });

        this.find(settings.extendFormOn).click(function(){
            thisForm.find(settings.fMsg).removeClass('ignore');
            thisForm.find("input[name='fMail']").removeClass('ignore');
            thisForm.find("input[name='fPhone']").addClass('ignore');
        });

        this.find(settings.extendFormOff).click(function(){
            thisForm.find(settings.fMsg).addClass('ignore');
            thisForm.find("input[name='fMail']").addClass('ignore');
            thisForm.find("input[name='fPhone']").removeClass('ignore');
        });

        this.validate(
            {
                rules: {
                    name: {
                        required:true
                    },
                    phone:{
                        required: true,
                        allPhoneFormat: true
                    },
                    email:{
                        required: true,
                        email:true
                    },
                    message: {
                        required: true
                    }
                },

                messages:{
                    email:{
                        required: "Введите E-mail",
                        email: "Неверный формат"
                    },
                    phone:{
                        required: "Введите телефон",
                        digits: "Введите номер в числовом формате"
                    },
                    name: {
                        required:"Введите имя"
                    },
                    message:{
                        required:"Введите сообщение"
                    }
                },
                
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass(errorClass).removeClass(validClass);
                        $(element.form).find("label[for=" + element.id + "]")
                    .addClass(errorClass);
                },
                
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass(errorClass).addClass(validClass);
                        $(element.form).find("label[for=" + element.id + "]")
                    .removeClass(errorClass);
                    
                    //Логика ввода либо телефона либо электронной почты в сразу расширенной форме
                    if (settings.extended == true) {
                        //console.log("a = " + $(element).attr('name') + " f = " + settings.fPhone.substring(1));
                        // Если заполнено корректно поле email - снимаем валидацию с номера телефона
                        if($(element).attr('name') == settings.fMail.substring(1)) {
                            $(settings.fPhone).addClass('ignore').removeClass('error');
                            $(settings.fPhone).parent().find('label').hide();
                        }
                        
                        // Если заполнено корректно поле телефона - снимаем валидацию с email
                        if($(element).attr('name') == settings.fPhone.substring(1)) {
                            $(settings.fMail).addClass('ignore').removeClass('error');
                            $(settings.fMail).parent().find('label').hide();
                        }
                    }
                },

                ignore: ".ignore",

                
                submitHandler: submit
                
            }
        );
        //this.find("input[name='phone']").mask("?(999) 999-99-99");

        function submit(form){
            if (settings.submitHandler) {
                $.post(
                    '/ajax-call-back.php?action=send2',
                    $(form).serialize(),
                    parseResponce);
            }
            else {
                form.submit();  
            }
        }

        function parseResponce(response) {
            thisForm.replaceWith('<div class="result">Спасибо, в ближайшее время наш менеджер свяжется с Вами.</div>')
        }
    };
})(jQuery);