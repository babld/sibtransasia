﻿
$(document).ready(function(){
	function cityIP(){
		$.getJSON('/user_info.js.php',
		
		function(result) {
			var ip = result.ip;
			  
			$.ajax({
				type: "GET",
				url: "http://ipgeobase.ru:7020/geo?ip="+ip,
				dataType: "xml",
				cache: false,
				success: function(xml) {
					var region = $(xml).find('region').text();
					var district = $(xml).find('district').text();
				   
					switch(district){
					  
						case "Сибирский федеральный округ":
							$(".phone_number .extra-number").html("8 (383) 207-88-60");
							$(".footer-address").html("Новосибирск, <a href='/contacts/ajax-show-map.php?id=29' class='fancybox fancybox.ajax marker'>пр-т Дзержинского, 1/1</a> <br/>5 этаж, офис 71");
							
							//Спецпредложение
							$(".special-whole-day").text("5 дней");
							$(".special-whole-price").text("340 000 рублей");
							$(".special-whole-city").text("Новосибирск");
						break;
						
						case "Центральный федеральный округ":
							$(".phone_number .extra-number").html("8 (499) 346-67-99");
							$(".footer-address").html("&nbsp;");
							
							//Спецпредложение
							$(".special-whole-day").text("9 дней");
							$(".special-whole-price").text("385 000 рублей");
							$(".special-whole-city").text("Москву");
						break;
						
						case "Северо-Западный федеральный округ":
							$(".phone_number .extra-number").html("8 (812) 424-33-13");
							$(".footer-address").html("&nbsp;");
							
							//Спецпредложение
							$(".special-whole-day").text("9 дней");
							$(".special-whole-price").text("385 000 рублей");
							$(".special-whole-city").text("Москву");
						break;	
						
						case "Уральский федеральный округ":
							$(".phone_number .extra-number").html("8 (343) 345-65-32");
							$(".footer-address").html("&nbsp;");
							
							//Спецпредложение
							$(".special-whole-day").text("7");
							$(".special-whole-price").text("360 000 рублей");
							$(".special-whole-city").text("Екатеринбург");
						break;
						default:
							$(".phone_number .extra-number").html("8 (383) 207-88-60");
							
							//Спецпредложение
							$(".special-whole-day").text("9 дней");
							$(".special-whole-price").text("385 000 рублей");
							$(".special-whole-city").text("Москву");
					}	
			
					switch(region){
						
						case "Омская область":
							$(".phone_number .extra-number").html("");
						break;
						
						case "Томская область":
							$(".phone_number .extra-number").html("8 (381) 297-20-30");
						break;														
					}  
				},
				error: function() { cityIP();  }
			});
		});
	}
	
    var oldIE;
    if ($('html').is('.ie6, .ie7, .ie8')) {
        oldIE = true;
    }

    if (oldIE) {
        // Here's your JS for IE..
    } else {
        cityIP();
    }


	$("#feedback-form").staFeedback({
		'extended' : true
	});

	$('.slider-block ul li').click(function()
	{
		$('.slider-block ul li').removeClass('active');
		$(this).addClass('active');
		var indx=$(this).index();
		$('.slider-block .big-pic .item').css('display','none');
		$('.slider-block .big-pic .pic-'+indx).fadeIn(2000);
		return false;
	});

    /* FEEDBACK FORM */
    jQuery.validator.addMethod("ruPhoneFormat", function (value, element) {
        return this.optional(element) || /\(\d{3}\) \d{3}\-\d{2}\-\d{2}?$/.test(value);
    }, "Введите корректно.");
	
	jQuery.validator.addMethod("allPhoneFormat", function (value, element) {
        return this.optional(element) || /^[0-9()\-+ ]+$/.test(value);
    }, "Введите корректно.");

    $("#call-back-2").staFeedback({
			'extended'	: true
		});
    /* // FEEDBACK FORM */
	
	$('.faq-list h3 a').click(function()
	{
		$('.faq-list li .panel').slideUp();
		$('.faq-list li').removeClass('active').addClass('no-active');
		if($(this).parent().parent().hasClass('no-active'))
		{
			$(this).parent().parent().removeClass('no-active').addClass('active');
			$(this).parent().parent().find('.panel').slideDown();
		}
		else
		{
			$(this).parent().parent().removeClass('active').addClass('no-active');
			$(this).parent().parent().find('.panel').slideUp();
		}				
		return false;
	});
	
	$("a[rel=lightbox]").fancybox();
	
	$('.fancybox').fancybox({
		padding: 15,
		scrolling: 'auto'
	});
	
	$('#slider').easySlider({a:4,auto:false,continuous:false,numeric:false,nextId:"slider-next",prevId:"slider-prev"});
	
	// service description
	var service_description = $('#services_description');
	
	$('#services a').on('mouseenter', function(){
		service_description.html( $(this).find('.description').html() );
	});
	
	// benefits images
	$('#benefits li span').on('mouseover', function(){
	
		var
		image = $(this).attr('data-position'),
		imageDescription = $('#benefits_description span');
		
		imageDescription.html( $(this).find('i').html() );
		$('#benefits_image img').fadeOut({duration: 10});
		$(image).fadeIn({duration: 10});
		
		return false;
	});
	
	// textfields 
	$('input[type="text"]').on('focus', function(){
		
		//var el = $(this);
		//if ( el.val() === el.attr('value') ) {
		//	el.val('');
			
		//}
		//el.attr('class', 'active');
		
	}).on('blur', function(){
		
		var el = $(this);
		if ( el.val() == '' || el.val() == ' ' ) {
			el.val( el.attr('value') );
		}
		/* WFT?
		el.removeAttr('class');
		*/
		
	});
	
	targetUrl = window.location.hash;
	if (targetUrl != '') {
		/*
		 * Если есть хэш
		 */
		$(targetUrl).attr('class', 'current');
		$('#prices a[href=' + targetUrl + '], #regions a[href=' + targetUrl + ']' ).attr('class', 'active');
		$(targetUrl + '-i').addClass('current');
	} else {
		/*
		 * Если нет хэша
		 */
		$('#offices li:first, #managers .manager:first').addClass('current');
		$('#prices a:first, #regions a:first').attr('class', 'active');
	}
	
	// contancts
	$('#regions a').on('click', function(){
		$('#regions a').removeAttr('class');
		$(this).addClass('active');
		var el= $(this);
		var target = el.attr('href');
		
		var offices = $('#offices li');
		
		$('#offices').fadeOut(300, function(){
			
			offices.removeClass('current');
			$(target).addClass('current');
			
			$(this).fadeIn();
		});
		
		var managers = $('#managers .manager');
		
		$('#managers').fadeOut(300, function(){
			managers.removeClass('current');
			$(this).find(target + '-i').addClass('current');
			$(this).fadeIn();
		});	
		//return false;
	});
	
	// prices
	
	$('#prices a').on('click', function(){
		
		$('#prices a').removeAttr('class');
		$(this).attr('class', 'active');
		var el = $(this);
		var target = el.attr('href');
		
		var offices = $('#offices li');
		
		$('#offices').fadeOut(300, function(){
			
			offices.removeAttr('class');
			$(target).attr('class', 'current');

			$(this).fadeIn();
		});
		
		$('#managers .manager').each(function() {
    		$(this).removeClass('current').fadeOut();
		});
		
		$('#managers').find(target+'-i').fadeIn();
		
		//return false;
	});
	
	$(".contacts .cLargeImg").click(function(){
		$this = $(this);
		
		$this.parent().find('.cLargeImg').addClass('fancybox');
		// Удаляем класс fancybox чтобы эта картинка не включалась в листалку в галерее
		$this.removeClass('fancybox');
		
		
		$this.parent().find(".large-photo a img").attr('src', $this.attr('data-bimage')).parent().attr('href', $this.attr('href'));
		return false;
	});
	
	$.fn.scrollToTop=function(options)
	{
        var settings = $.extend({
            'height' : 300
        }, options);

		if($(window).scrollTop()>settings.height) {
			$(this).fadeIn("slow")
		}

		var scrollDiv=$(this);
		$(window).scroll(function()
		{
			if($(window).scrollTop()<settings.height)
			{
				$(scrollDiv).fadeOut("slow")
			}
			else
			{
				$(scrollDiv).fadeIn("slow")
			}
		});
		$(this).click(function()
		{
			$("html, body").animate({scrollTop:0},"slow")
			return false;
		})
	}
	$("#footer .str").scrollToTop();
	
	
});