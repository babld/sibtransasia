<?php
	define("NO_KEEP_STATISTIC", true);
	define("NOT_CHECK_PERMISSIONS", true);
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
	
	function getExtension($filename) {
		$path_info = pathinfo($filename);
		return $path_info['extension'];
	}
	
	/*
	Uploadify
	Copyright (c) 2012 Reactive Apps, Ronnie Garcia
	Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
	*/
	
	// Define a destination
	$targetFolder = 'images/uploads'; // Relative to the root
	
	$verifyToken = md5('unique_salt' . $_POST['timestamp']);
	
	if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
		$tempFile = $_FILES['Filedata']['tmp_name'];
		$targetPath = $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/" . $targetFolder;
		
		$extension = getExtension($_FILES['Filedata']['name']);
		
		$_FILES['Filedata']['name'] = rand(1, 1000000) . "." . $extension;
		
		$targetFile = rtrim($targetPath,'/') . '/' . $_FILES['Filedata']['name'];
		
		echo $_FILES['Filedata']['name']; 
		
		// Validate the file type
		$fileTypes = array('jpg','jpeg','gif','png', 'JPG', 'pdf', 'PDF', 'doc', 'DOC', 'docx', 'DOCX', 'txt', 'TXT', 'xls', 'XLS', 'xlsx', 'XLSX'); // File extensions
		$fileParts = pathinfo($_FILES['Filedata']['name']);
		
		if (in_array($fileParts['extension'],$fileTypes)) {
			move_uploaded_file($tempFile,$targetFile);
		} else {
			echo 'Invalid file type.';
		}
	}
?>