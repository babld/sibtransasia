<?php
	
	class Paginator {
		public function getPages($coundDisplayPages = 20, $totalPages, $currentPage, $query, $deltaAfter = 3, $deltaBefore = 3) {

			$totalPages = ($totalPages > 512) ? 512 : $totalPages; 
			
			if($currentPage > 1) {?>
				<a href="<?=$_SERVER["PHP_SELF"]?>?<?=$query?>&page=<?=$currentPage - 1?>">&lt; Предыдущая</a> <?
			}
			
			if($totalPages > $coundDisplayPages) {
				if($currentPage - $deltaBefore <= 1) {
					for($i = 1; $i <= $deltaAfter + $deltaBefore; $i++) { ?>
						<a href="<?=$_SERVER["PHP_SELF"]?>?<?=$query?>&page=<?=$i?>" <?=($i == $currentPage) ? 'class="active"' :""?>><?=$i?></a> | <?
					}?> ... <?
					
					for($i = $totalPages - $deltaAfter; $i <= $totalPages; $i++){ ?>
						| <a href="<?=$_SERVER["PHP_SELF"]?>?<?=$query?>&page=<?=$i?>"><?=$i?></a><?
					}
					
					
				} else if($currentPage - $deltaBefore > 1 and $currentPage + $deltaAfter < $totalPages - 1){ ?>
					<a href="<?=$_SERVER["PHP_SELF"]?>?<?=$query?>&page=1">1</a> |<? 
					
					if($currentPage - $deltaBefore > 2) { ?> ... |<? }
					
					for($i = $currentPage - $deltaBefore; $i <= $currentPage + $deltaAfter; $i++ ) { ?>
						<a href="<?=$_SERVER["PHP_SELF"]?>?<?=$query?>&page=<?=$i?>" <?=($i == $currentPage) ? "class='active'" :""?>><?=$i?></a> | <?
					}
					
					if($currentPage + $deltaAfter < $totalPages) { ?> ... <?} ?>
					<a href="<?=$_SERVER["PHP_SELF"]?>?<?=$query?>&page=<?=$totalPages?>"><?=$totalPages?></a><?
					
				} else if($currentPage + $deltaAfter >= $totalPages - 1) { ?>
					<a href="<?=$_SERVER["PHP_SELF"]?>?<?=$query?>&page=1">1</a> | ... <? 
					for($i = $totalPages - $deltaBefore - $deltaAfter; $i <= $totalPages; $i++){ ?>
						| <a href="<?=$_SERVER["PHP_SELF"]?>?<?=$query?>&page=<?=$i?>" <?=($i == $currentPage) ? "class='active'" :""?>><?=$i?></a> <?
					}
				}
				
			} else {
				for($i = 1; $i <= $totalPages; $i++) { ?>
					<a href="<?=$_SERVER["PHP_SELF"]?>?<?=$query?>&page=<?=$i?>" <?=($i == $currentPage) ? 'class="active"' :""?>><?=$i?></a><?
				}
			}
			
			if($currentPage + 1 <= $totalPages) {?>
				<a href="<?=$_SERVER["PHP_SELF"]?>?<?=$query?>&page=<?=$currentPage + 1?>">Следующая &gt;</a> <?
			}
		}   
	}
?>