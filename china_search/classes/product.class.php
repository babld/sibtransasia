<?php

	class Product {

		public function getSearchField($query = null, $example = null, $minPrice = false, $maxPrice = false, $sortType = null) {
			$sortTypes = array(
					"MAIN" => "Лучшее совпадение",
					"PP_A" => "Цена по возрастанию",
					"PP_D" => "Цена по убыванию",
					"TC_D" => "Кол-во заказов",
					"SC_D" => "Рейтинг продавца"
				);?>
			<form id="china_search-mainForm" class="searchbar-form w-clear" method="get" action="/china_search/" onsubmit="yaCounter23212990.reachGoal('china_search'); return true;">
				<input id="china_search-form-query" type="text" placeholder="Введите название товара..." value="<?=$query?>" name="query" class="search-key"/>
				<input class="price-range" name="min-price" placeholder="Цена от, $" type="text" class="" value="<?=$minPrice?>"/>
				<input class="price-range" name="max-price" placeholder="Цена до, $" type="text" class="" value="<?=$maxPrice?>"/>
				
				<select name="sortType" class="china_search-select"><?
					foreach($sortTypes as $key => $value){ ?>
						<option value="<?=$key?>" <?=($key == $sortType) ? "selected=\"selected\"" : ""?>><?=$value?></option><?
					} ?>
				</select>
				
				<input type="submit" class="search-button" value="Найти в Китае"/>
				<div class="clear"></div><?
				if($example) {?>
					<div class="example">
						Например, <a class="china_search-query-expamle">деревообрабатывающий станок</a>
					</div><?
				}?>
			</form>

			<div class="disclaimer">
				<span class="attention">Внимание!</span> К поставке принимается партия товара не менее 100 кг на одну товарную группу.
				<span class="more-information">
					Условия поставки
					<i>Поставки, принимаемые компанией к перевозке, должны составлять не менее 100 кг на одну товарную группу. Доставка осуществляется
					автотранспортом, транзитом через Казахстан, до логистического терминала компании на границе с Казахстаном, в составе сборного груза или,
					при наличии достаточного объема в составе, цельной партии. Весь товар, принимаемый к поставкам, подлежит таможенному оформлению.</i>
				</span>
				<script>
					opacityValue = 1;
					$(".disclaimer .more-information").click(function(){
						$(this).find("i").show().animate({opacity: opacityValue},  800, "swing", function(){
							opacityValue = (opacityValue == 1) ? 0 : 1;
							if (opacityValue) $(this).hide();
						});
					});
					
					$(".china_search-query-expamle").click(function(){
						var form = $("#china_search-mainForm");
						$("#china_search-form-query").val(this.text);
						
					});
				</script>
			</div> <?
		}
		
		public function getImage($img, $imgSize) {
			echo "<img src='$img' $imgSize alt='' title=''/>";
		}
		
		public function getDescription($id, $lang) {
			$langPar = ($lang == "en") ? "site=glo" : "";
			
			$html_desc = file_get_html("http://m.aliexpress.com/item-desc/$id.html?$langPar");
			
			foreach($html_desc->find("div.description") as $elementItem){
				$str = $elementItem->innertext;
				
				// Обработка полученного html кода регулярными выражениями, замена внешних ссылок алиэкспресс на внутренние
				$par1 = "(href=\"http:\/\/www.aliexpress.com\/[-a-zA-Z]{0,}\/)";
				$par2 = "([a-zA-z0-9\/-]{0,}\/)([0-9]{0,})(.html)";
				$par3 = "([0-9]{0,})([-a-zA-Z0-9]{0,})(.html)";
				
				$str = preg_replace("/$par1$par2/",  "href=\"/china_search/product.php?id=$3", $str);
				$str = preg_replace("/$par1$par3/",  "href=\"/china_search/product.php?id=$2", $str);
				
				// Выкашиваем пустые теги <p></p> <ul></ul>
				$str = preg_replace("/(<p>\s&nbsp;\s<\/p>)/", "", $str);
				$str = preg_replace("/(<p>\s<span>&nbsp;<\/span><\/p>)/", "", $str);
				$str = preg_replace("/(\s{0,}&nbsp;{0,}\s{0,})/", "", $str);
				$str = preg_replace("/(<ul><\/ul>)/", "", $str);
				echo $str;
			}
		}
	}


?>