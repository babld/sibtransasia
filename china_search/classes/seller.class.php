<?php
	class Seller {	
		public function getElement($html, $once = false) {
			foreach($html as $elementItem) {
				if(!$once) {
					echo $elementItem->innertext;
				}
				else {
					echo "<div>" . $elementItem->innertext . "</div>";
				}
			}
		}
	}

?>
