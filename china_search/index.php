<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
	$APPLICATION->SetPageProperty("description", "Онлайн сервис поиска товаров и оборудования в Китае");
	$APPLICATION->SetPageProperty("keywords", "Поиск товаров, Китай, Поиск оборудования");
	$APPLICATION->SetPageProperty("title", "Поиск товаров в Китае");

	$queryOrig	= (isset($_GET["query"]) and !empty($_GET["query"])) ? $_GET["query"] : null;
	$companyIds = (isset($_GET["companyIds"]) and !empty($_GET["companyIds"])) ? $_GET["companyIds"] : null;
	$paginatorQuery = "";
	
	$minPrice		= $_REQUEST["min-price"] ? $_REQUEST["min-price"] : null;
	$maxPrice		= $_REQUEST["max-price"] ? $_REQUEST["max-price"] : null;
	$priceRange		= "";
	
	$sortType		= $_REQUEST["sortType"] ? $_REQUEST["sortType"] : "MAIN";
	
	$searchExampleText = ($queryOrig or $companyIds) ? 0 : 1;
	
	require("classes/product.class.php");
	
	$page = new Product(); ?>

	<div class="china-search">
		<h1><?$APPLICATION->ShowTitle()?></h1><?
		
		$page->getSearchField($queryOrig, $searchExampleText, $minPrice, $maxPrice, $sortType);
		
		if(!$queryOrig and !$companyIds){?>
			<div class="inner_page"><?
				$APPLICATION->IncludeComponent("bitrix:news.list", "china_search.index-text",
					array(
						"IBLOCK_ID" => "15",
					)
				);?>
			</div><?
		} else {
			require("classes/paginator.class.php");
			$page 			= (isset($_GET["page"]) and !empty($_GET["page"])) ? $_GET["page"] : "1";
			$query			= str_replace(" ", "-", $queryOrig);
			$catId			= $_REQUEST["catId"];
			$categoryIds	= "&categoryIds=$catId";
			
			if($minPrice or $maxPrice) {
				$priceRange = $minPrice . "-" . $maxPrice;
			}
			
			$paginator = new Paginator();
			#http://m.aliexpress.com/search/productListJson.htm?keywords=&shippingCountry=RU&viewtype=1&page=
			$i = 0;
			
			if(!$companyIds) {
				$jsonUrl = "http://m.aliexpress.com/search/productListJson.htm?keywords="
						. $query
						. "&shippingCountry=RU&viewtype=1&page=$page&priceRange=$priceRange&sortType=$sortType";
				$paginatorQuery = "query=$queryOrig&min-price=$minPrice&max-price=$maxPrice&sortType=$sortType";
			} else {
				$jsonUrl = "http://m.aliexpress.com/search/productListJson.htm?companyIds=$companyIds&shippingCountry=RU&viewtype=1&page=$page";
				$paginatorQuery = "companyIds=$companyIds";
			}
			
			$jsonUrl = file_get_contents($jsonUrl);
			$jsonData = json_decode($jsonUrl, true);
			
			$totalPages = $jsonData['totalPage'];

			if($jsonData["productList"]) {
				$i = 1; ?>
				<table class="china-search-table" cellspacing="20" width="930"><tr> <?
					foreach($jsonData["productList"] as $item) {
						$productPrice = ($item["discount"] > 0) ? $item["promoMaxPrice"] : $item["max_product_price"]; ?>
						<td width="25%" class="box-shadow"><?
							$id		= (int)$item["id"];
							$link	= "product.php?id=$id"; ?>
							<div class="china-search-img">
								<a href="<?=$link?>"><img src="<?=$item["img_url"]?>"/></a>
							</div>
							<div class="china-item-price">
								<span class="price-title">Цена в Китае:</span>
								<span class="price-usd">$<?=$productPrice?></span>
							</div>
							<a href="orderwindow.php" data-id="<?=$id?>" class="china-item-order"><i></i>Доставить</a>
							<div class="clear"></div>
							<div class="china-search-title">
								<a href="<?=$link?>"><?=$item["subject"]?></a>
							</div>
						</td> <?
						
						if ($i % 20 == 0 and $i != 1){ ?></tr><? }
						else if($i % 4 == 0 and $i != 1) { ?></tr><tr><? }
						
						$i++;
					} ?>
				</table>

				<div class="navigation-pages"><?
					$paginator->getPages(20, $totalPages, $page, $paginatorQuery); ?>
				</div>
				<script>					
					$('.china-item-order').on("click", function (e) {
						e.preventDefault(); // avoids calling preview.php
						var $this = $(this);
						params = "img=" +  $this.parent().find("img").attr("src")
								 + "&title=" + $this.parent().find(".china-search-title a").text()
								 + "&price=" + $this.parent().find(".china-item-price .price-usd").text()
								 + "&product=" + "http://" + document.location.host + "/china_search/product.php?id=" + $this.attr("data-id");
						$.ajax({
							type: "POST",
							cache: false,
							url: this.href,
							data: params,
							success: function (data) {
								// on success, post (preview) returned data in fancybox
								$.fancybox(data, {
									// fancybox API options
									fitToView: false,
									autoSize: false,
									closeClick: false,
									padding: 0,
									margin: 0,
									width:	"auto",
									height: "auto"
								}); // fancybox
							} // success
						}); // ajax
					}); // on
				</script> <?
			} else {?>
				<h2>По запросу "<?=$queryOrig?>" ничего не найдено!</h2> <?	
			}
		} ?>
	</div> <?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
