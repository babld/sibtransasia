<?php

	$file_button = "Добавить файлы";

	$img_src 	= (isset($_POST["img"])		and !empty($_POST["img"]))		? $_POST["img"]		: "/bitrix/templates/siberia.trans.1.0/images/no-image.jpg";
	$title		= (isset($_POST["title"])	and !empty($_POST["title"]))	? $_POST["title"]	: "Нет названия";
	$price		= (isset($_POST["price"])	and !empty($_POST["price"]))	? $_POST["price"]	: "Цену уточняйте у менеджеров";
	$product	= (isset($_POST["product"])	and !empty($_POST["product"]))	? $_POST["product"]	: false; 
	
	if($product): ?>
		<div class="orderwindow-wrapper">
			<div class="orderwindow__img-wrap">
				<img class="orderwindow__img-main" src="<?=$img_src?>" alt="<?=$title?>" title="<?=$title?>"/>
			</div>
			<div class="orderwindow__announce">
				<div class="orderwindow__announce-head">Описание товара</div>
				<div class="orderwindow__announce-title"><?=$title?></div>
				<div class="orderwindow__price">
					<span class="orderwindow__price-head">Цена: </span>
					<span class="orderwindow__price-cost">US <?=$price?> / шт.*</span>
					<ul class="orderwindow__price-notice">
						<li>Цена не является окончательной и ее необходимо уточнить у поставщика</li>
					</ul>
				</div>
			</div>
			<div class="clear"></div>
			<form class="orderwindow__form" method="POST" enctype="multipart/form-data" action="/services/emailsend.php" onsubmit="yaCounter23212990.reachGoal('china_search_lightbox');yaCounter23212990.reachGoal('all'); return true;">
				<input type="hidden" name="product" value="<?=$product?>"/>
				<input type="hidden" name="subject" value="с поиска по Китаю"/>
				<div class="orderwindow__form-head">Нужно найти товар в Китае и привезти в Россию?</div>
				<div class="orderwindow__form-fields">
					<div class="orderwindow__form-idents">
						<div class="inpt">
							<input class="orderwindow__form-idents-item" type="text" name="name" placeholder="Имя.."/>
						</div>
						<div class="inpt">
							<input class="orderwindow__form-idents-item" type="text" name="phone" placeholder="Телефон.."/>
						</div>
						<div class="inpt">
							<input class="orderwindow__form-idents-item" type="text" name="email" placeholder="Электронная почта.."/>
						</div>
					</div>
					<div class="orderwindow__form-textarea-wrap">
						<textarea name="message" class="orderwindow__form-textarea" placeholder="Введите описание товара и параметры поставки"></textarea>
					</div>
					<div class="clear"></div>
				</div>
				<div class="orderwindow__form-files-wrap">
					<div class="orderwindow__form-files-head">Добавьте файлы при необходимости</div>
					<div class="orderwindow__form-upload-files">
						<div class="file_upload">
							<div>Файл не выбран</div>
							<button type="button" class="as-button white"><?=$file_button?></button>
							<input type="file" name="filename[]"/>
						</div>	
					</div>
					<div class="clear"></div>
					<a class="buttons__more-files" href="javascript:void(0)">Прикрепить еще</a>
				</div>
				<div class="orderwindow__form-submit-wrap w-clear">
					<div class="orderwindow__form-submit-head">
						Отправьте запрос на поиск товаров нашим специалистам. <strong>Доверьте работу профессионалам!</strong>
					</div>
					<input type="submit" name="submit" class="as-button" value="Найти и доставить"/>	
				</div>
				<div class="orderwindow__form-notice">
					Внимание! Мы сотрудничаем только с <span>юридическими лицами</span>
				</div>
			</form>
			<script>
				$(".orderwindow__form .file_upload").addAttachButton();
				$(".orderwindow__form .buttons__more-files").on("click", function(){
					$(".orderwindow__form-upload-files").append('<div class="file_upload">\
							<div>Файл не выбран</div>\
							<button type="button" class="as-button white"><?=$file_button?></button>\
							<input type="file" name="filename[]"/>\
							<i class="delete_attach_file"></i>\
						</div>');
					$(".orderwindow__form .file_upload").each(function(){
						$(this).addAttachButton();
					});
				});
				$(".orderwindow__form").staFeedback({"submitHandler" : false});
			</script>
			
		</div> <?
	endif;
?>
	
	