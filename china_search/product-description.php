<?php
	
	if(isset($_POST["id"]) and !empty($_POST["id"])) {
		include("lib/simple_html_dom.php");
		require("classes/product.class.php");
		
		
		$id = $_POST["id"];
		$lang = (isset($_POST["lang"]) and !empty($_POST["lang"])) ? $_POST["lang"] : "";
		
		$product = new Product();
		
		$product->getDescription($id, $lang);
	}
?>