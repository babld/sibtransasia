<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
	$APPLICATION->SetPageProperty("robots", "noindex");
	
	$id = (isset($_REQUEST["id"]) and !empty($_REQUEST["id"])) ? $_REQUEST["id"] : null;
	
	if($id){
		include("lib/simple_html_dom.php");
		require("classes/product.class.php");
		$page = new Product();
		
		$fancyboxClass = "ali-fancybox";
		$size = 400;
		$i = 0;?>
		<div class="china-search"> <?
			
			$page->getSearchField();?>
			<div class="ali-product"><?
				if($html = file_get_html("http://m.aliexpress.com/item/$id.html?site=glo")){
					foreach($html->find("div.product-name") as $elementItem){
						$pageTitle = $elementItem->innertext; ?>
						<h1><?=$pageTitle?></h1> <?
						$APPLICATION->SetTitle("$pageTitle | СибирьТрансАзия");
						$APPLICATION->SetPageProperty("description", "$pageTitle");
					} ?>
					<div class="carousel_wrap">
						<div class="image" id="foo1"> <?php
							foreach($html->find("div.product-images div.item") as $element) {
								foreach($element->find("img") as $elementItem) {
									$imgPreview = ($elementItem->src != 'about:blank') ? $elementItem->src : $elementItem->{'image-src'};
									$imgThumb = str_replace ("_250x250.jpg", "_50x50.jpg", $imgPreview);
									$img = str_replace ("_250x250.jpg", "", $imgPreview);
									
									if($elementItem->src != 'about:blank'){ ?>
										<div class="large-image">
											<a href="<?=$img?>" class="<?=$fancyboxClass?> large-image-src" rel="gallery"> <?
												$imageProp = getimagesize($img);
												$imgSize = ($imageProp[0] > $imageProp[1]) ? "width=$size" : "height=$size";
												$page->getImage($img, $imgSize);
												
												# Картинка для лайтбокс заказа
												$imgOrderWindow = $img . "_220x220.jpg";
												?>
											</a>
										</div>
										<div class='thumbs w-clear'> <?
									}
									echo "<div class='thumb img" . ((!$i) ? " selected" : "") . "'><a href='$img' rel='gallery' class='" . ((!$i++) ? "" : $fancyboxClass ). " ali-gallery-thumb'>";
									$page->getImage($imgThumb);
									echo "</a></div>";
								}
							}?>
							</div> <!-- Thumbs -->
						</div>
						<div class="clear"></div>
					</div>
				
					<div class="product-more">
						<div class="product-prop">
							<div class="product-prop-title">Цена товара в Китае:</div>
							<div class="product-prop-cost"> <?
								foreach($html->find("ul li.product-price") as $elementItem) {
									echo $elementItem->innertext; 
								}?>
							</div>
						</div>
						<div class="ali-order">
							<a href="orderwindow.php" class="china-item-order box-shadow"><i></i>Найти и доставить</a>
							<a href="javascript:void(0)" class="lang-description box-shadow" data-lang="ru">Показать на русском <i></i></a>
						</div>
						<div class="clear"></div>
						<div class="product-prop">
							<div class="product-prop-title">Спецификация</div> <?
							foreach($html->find("ul li[id=js_item_specifics]") as $elementItem)
								echo $elementItem->innertext; ?>
						</div>
						<div class="product-prop">
							<div class="product-prop-title">Продавец.</div> <?
							foreach($html->find("li[id=js_supplier_btn]") as $elementItem){
								$str = $elementItem->innertext;
								
								$str = preg_replace("/(href=\"http:\/\/m.aliexpress.com\/getSellerInfo.htm)/", "href=\"/china_search/seller.php", $str);
								$str = preg_replace("/(href=\"http:\/\/m.aliexpress.com\/store\/storeHome.htm)/", "href=\"/china_search/seller.php", $str);
								echo $str;
							}?>
						</div>
					</div>

					<script>
						$('.china-item-order').on("click", function (e) {
							e.preventDefault(); // avoids calling preview.php
							var $this = $(this);
							params = "img=<?=$imgOrderWindow?>"
									 + "&title=<?=$pageTitle?>"
									 + "&price=" + $this.parent().parent().find(".product-prop .product-prop-cost").text()
									 + "&product=" + document.location.href;
							
							$.ajax({
								type: "POST",
								cache: false,
								url: this.href,
								data: params,
								success: function (data) {
									// on success, post (preview) returned data in fancybox
									$.fancybox(data, {
										// fancybox API options
										fitToView: false,
										autoSize: false,
										closeClick: false,
										padding: 0,
										margin: 0,
										width:	"auto",
										height: "auto"
									}); // fancybox
								} // success
							}); // ajax
						}); // on
						
						$(".lang-description").click(function(e){
							$this = $(this);
							e.preventDefault();
							
							$this.addClass("ajax-loader").attr("disabled", "disabled");
							$(".description").addClass("ajax-loader");
							
							// Смотрим какой текущий язык
							cur_lang = $this.data("lang");
							tog_lang = cur_lang == "ru" ? "en" : "ru";
							
							$.post("/china_search/product-description.php",
								{"id": <?=$id?>, "lang": cur_lang},
								function(response){
									button_text = cur_lang == "ru" ? "Показать на английском" : "Показать на русском";
									button_text += "<i class='"+ tog_lang +"'></i>";
									$this.html(button_text);
									//Меняем язык в кнопке
									$this.data("lang", tog_lang);
									
									$(".description").html(response).removeClass("ajax-loader");
									$this.removeClass("ajax-loader").removeAttr("disabled");
								});
							return false;
						});
						
						$(".<?=$fancyboxClass?>").fancybox({
							padding: 15,
							scrolling: 'auto'
						})
						
						$("#foo1 .thumb .ali-gallery-thumb").click(function(){
							var img = new Image();
							var src = $("img", this).attr( "src" );
							imgPar = "";
							var fancyboxClass = "<?=$fancyboxClass?>";
							
							$(this).parent().parent().parent().find('.ali-gallery-thumb').addClass(fancyboxClass);
							$(this).removeClass(fancyboxClass);
							
							img.src = src;
							
							imgPar = (img.width > img.height) ? "width" : "height";
							
							src = src.replace(".jpg_50x50.jpg", ".jpg");
							
							$(this).parent().parent().parent().find(".img.selected").removeClass("selected");
							$(this).parent().addClass("selected").parent().parent().find(".large-image-src").attr("href", src).find("img")
								.removeAttr("width").removeAttr("height").attr(imgPar, 400).attr("src",src);
							return false;
						});
					</script>
					<div class="clear"></div>
					<h2>Подробное описание товара</h2>
					<div class="description"> <?
						$page->getDescription($id, "en"); ?>
					</div> <?
				} else { ?>
					404 <?
				} ?>
			</div>
		</div>	
		<?
	} else {
		header("Location:/china_search/");
	}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
