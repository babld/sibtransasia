<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
	$APPLICATION->SetTitle("Продавец | Поиск товаров у китайских поставщиков | СибирьТрансАзия");
	$APPLICATION->SetPageProperty("robots", "noindex");
	
	$adminSeq	= (isset($_REQUEST["sellerAdminSeq"]) and !empty($_REQUEST["sellerAdminSeq"])) ? $_REQUEST["sellerAdminSeq"] : null;
	$productId	= (isset($_REQUEST["productId"]) and !empty($_REQUEST["productId"])) ? $_REQUEST["productId"] : null;
	$numPage	= (isset($_REQUEST["page"]) and !empty($_REQUEST["page"])) ? $_REQUEST["page"] : 1;
	
	if($adminSeq) {
		include("lib/simple_html_dom.php");
		require("classes/seller.class.php");
		require("classes/product.class.php");
		require("classes/paginator.class.php");

		$page = new Product();
		$paginator = new Paginator();
		
		$infoUrl		= "http://m.aliexpress.com/getSellerInfo.htm?adminSeq=$adminSeq&productId=$productId";
		$reviewsUrl		= "http://m.aliexpress.com/getSiteEvaluationListAjax.htm?adminSeq=$adminSeq&page=$numPage";?>
		
		<div class="seller-page china-search"><?
			$page->getSearchField();
			if($html = file_get_html($infoUrl)) {
				$seller = new Seller(); ?>
				<a href="/china_search/product.php?id=<?=$productId?>">Назад к товару</a>
				<h1 class="comp-name">
					Продавец: 
					<?$seller->getElement($html->find("div.main div.comp-name"));?>
				</h1>
				<div class="comp-detail">
					<?$seller->getElement($html->find("div.main div.comp-detail"), true);?>
				</div>
				<div class="comp-rating">
					<?$seller->getElement($html->find("div.main div.feedback"));?>
				</div>
				<div class="comp-positive">
					<?$seller->getElement($html->find("div.main div.positive"));?>
				</div>
				<div class="feedback-history">
					<?$seller->getElement($html->find("div.main div.fed-his"));?>
				</div>
				<div class="seller-other-items"><?
					foreach($html->find("#js-sellers-other-items") as $elementItem) {
						$str = preg_replace("/(href=\"http:\/\/m.aliexpress.com\/search.htm)/", "href=\"/china_search/", $elementItem->innertext);
						echo $str;
					}?>
				</div><?
			}

			$reviewsJson = file_get_contents($reviewsUrl);
			$jsonData = json_decode($reviewsJson, true);?>

			<div class="feedbacks">
				<div class="feedback-title">Отзывы</div>
				<ul class="feedbacks-list"><?
					foreach($jsonData["evaluationList"]["evaViewList"] as $item) { ?>
						<li>
							<span class="star star-s"><span style="width:<?=$item["buyerEval"]?>%;"></span></span>
							<div class="feedback-text"><?=$item["buyerFeedback"]?></div>
							<div class="feedback-pic"></div>
							<div class="feedback-detail w-clear">
								<div class="date-buyer"><?=$item["gmtValid"]?> от <?=$item["buyerAnonymousName"]?></div><?
								if($item["buyerEvaluationLevel"] > 0) { ?>
									<div class="buyer-level">
										<img src="http://i02.i.aliimg.com/wimg/feedback/icon/<?=$item["buyerEvaluationLevel"]?>-b.gif" title="This is the Feedback Symbol for Feedback Scores from 10-29." alt="">
									</div><?
								}?>

								<div class="buyer-state">
									<img src="http://i02.i.aliimg.com/wimg/mobile/single/country/s/<?=$item["buyerCountry"]?>.gif">
								</div>
							</div>
						</li><?
					}?>
				</ul>
			</div> <?
			$totalPages = $jsonData['evaluationList']['totalPage'];?>
			<div class="navigation-pages"><?
				$paginator->getPages(20, $totalPages, $numPage, "productId=$productId&adminSeq=$adminSeq");?>
			</div>
		</div><?
	} else {
		header("Location: /china_search/");
	}
	
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>