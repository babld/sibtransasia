<?php
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
$id=intval($_REQUEST['id']);
$dbRes=CIBlockElement::GetByID($id);
if($arRes=$dbRes->GetNext()):
$db_props=CIBlockElement::GetProperty($arRes["IBLOCK_ID"], $arRes["ID"], array("sort" => "asc"), Array("CODE"=>"MAP"));
$ar_props=$db_props->Fetch();
?>
    <script type="text/javascript">
        function init () 
		{
			var myMap = new ymaps.Map("map", 
			{
					center: [<?=$ar_props["VALUE"]?>],
					zoom: 16
			});
			myMap.controls.add('zoomControl').add('typeSelector').add('mapTools');

			myPlacemark = new ymaps.Placemark([<?=$ar_props["VALUE"]?>]);
			myMap.geoObjects.add(myPlacemark);
        }
		ymaps.ready(init);
    </script>
	<div class="ajax-map">
		<h2><?=$arRes['NAME']?></h2>
		<div id="map"></div>
	</div>
<?endif;?>