<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require($_SERVER["DOCUMENT_ROOT"] . SITE_TEMPLATE_PATH ."/head.php");
$APPLICATION->SetPageProperty("description", "Группы товаров, которые перевозит ГК СибирьТрансАзия");
$APPLICATION->SetPageProperty("keywords", "товары");
$APPLICATION->SetPageProperty("title", "Группы товаров");
$APPLICATION->SetTitle("Товары");?>

<div class="layout-width">
	<div class="rubric-page">
		<div id="sidebar">
			<?$APPLICATION->IncludeFile(
				SITE_TEMPLATE_PATH.'/blocks/goods.side.php',
				array(),
				array('MODE'=>'php','SHOW_BORDER'=>false)
			);?>
		</div>
		<div class="content">
			<?$APPLICATION->IncludeComponent("bitrix:news", "goods", array(
				"IBLOCK_TYPE" => "goods",
				"IBLOCK_ID" => "9",
				"NEWS_COUNT" => "7",
				"USE_SEARCH" => "N",
				"TAGS_CLOUD_ELEMENTS" => "150",
				"PERIOD_NEW_TAGS" => "365",
				"USE_RSS" => "N",
				"USE_RATING" => "N",
				"USE_CATEGORIES" => "N",
				"USE_REVIEW" => "N",
				"USE_FILTER" => "N",
				"SORT_BY1" => "ACTIVE_FROM",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"CHECK_DATES" => "Y",
				"SEF_MODE" => "Y",
				"SEF_FOLDER" => "/goods/",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"SET_TITLE" => "Y",
				"SET_STATUS_404" => "Y",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"USE_PERMISSIONS" => "N",
				"PREVIEW_TRUNCATE_LEN" => "",
				"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
				"LIST_FIELD_CODE" => array(
					0 => "TAGS",
					1 => "",
				),
				"LIST_PROPERTY_CODE" => array(
					0 => "",
					1 => "",
				),
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"DISPLAY_NAME" => "N",
				"META_KEYWORDS" => "META_KEYWORDS",
				"META_DESCRIPTION" => "META_DESCRIPTION",
				"BROWSER_TITLE" => "META_TITLE",
				"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
				"DETAIL_FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"DETAIL_PROPERTY_CODE" => array(
					0 => "",
					1 => "GALLERY",
					2 => "",
				),
				"DETAIL_DISPLAY_TOP_PAGER" => "N",
				"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
				"DETAIL_PAGER_TITLE" => "Страница",
				"DETAIL_PAGER_TEMPLATE" => "",
				"DETAIL_PAGER_SHOW_ALL" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => "",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "Y",
				"DISPLAY_DATE" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"DISPLAY_AS_RATING" => "rating",
				"FONT_MAX" => "50",
				"FONT_MIN" => "10",
				"COLOR_NEW" => "3E74E6",
				"COLOR_OLD" => "C0C0C0",
				"TAGS_CLOUD_WIDTH" => "100%",
				"USE_SHARE" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"SEF_URL_TEMPLATES" => array(
					"news" => "",
					"section" => "",
					"detail" => "#ELEMENT_CODE#/",
				)
				),
				false
			);?>
		</div>
		<div class="clear"></div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>