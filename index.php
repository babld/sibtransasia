<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
	
<div class="index-header-block">
	<?require($_SERVER["DOCUMENT_ROOT"] . SITE_TEMPLATE_PATH ."/head.php");?>
	<div class="index-lead-text">
		<div class="index-lead-text-container">
			<p><b>Компания СибирьТрансАзия</b> &mdash; профессиональный оператор внешнеэкономической деятельности, специализирующийся на оказании комплексных
			услук высокого качества по доставке
			и таможенному оформлению товаров из Китая в Россию</p>
			<p><b>Наша работа с клиентом</b> &mdash; это, прежде всего, индивидуальный подход. С каждым Клиентом работает индивидуальный менеджер.
			Мы не ограничены определенными схемами перевозок. В каждом случае специалисты нашей компании предлагают оптимальный путь доставки товара,
			оптимизацию финансовых затрат, минимизацию рисков и сроков грузоперевозки.</p>
		</div>
	</div>
	<div class="shadow-bottom-block"></div>
</div>

<div class="index-profit-block">
	<div class="index-profit-block-container layout-width">
		<h2 class="index-block-header">Почему работать с нами выгодно?</h2>
		<div class="index-profit-items">
			<div class="index-profit-item ship">
				<div class="index-profit-item-title">Быстрая доставка</div>
				<p>Сборные грузы до Новосибирска за 14 дней, цельные партии за 9 дней.</p>
			</div>
			<div class="index-profit-item cost">
				<div class="index-profit-item-title">Низкие цены</div>
				<p>40"" контейнера до Новосибирска 245 000 рублей.</p>
			</div>
			<div class="index-profit-item drop">
				<div class="index-profit-item-title">Работа без посредников</div>
				<p>Мы реализуем услугу полностью. Зачем платить посредникам?</p>
			</div>
			<div class="index-profit-item guarant">
				<div class="index-profit-item-title">Гарантия цен</div>
				<p>Цены на всех этапах маршрута будут Вам предоставлены еще до поставки</p>
			</div>
			<div class="index-profit-item pack">
				<div class="index-profit-item-title">Полный пакет документов</div>
				<p>По окончанию поставки Вы получаете все необходимые документы для поставки товара на учет</p>
			</div>
			<div class="index-profit-item logist">
				<div class="index-profit-item-title">Удобная логистика</div>
				<p>Доставка до Вашего склада по всей России</p>
			</div>
			<div class="index-profit-item infr">
				<div class="index-profit-item-title">Своя инфраструктура</div>
				<p>Свой СВХ позволяет предоставлять качественные услуги по минимальным ценам</p>
			</div>
			<div class="index-profit-item skill">
				<div class="index-profit-item-title">Опытные сотрудники</div>
				<p>Сотрудники имеют огромный опыт работы в сфере внешне-экономической деятельности</p>
			</div>
			<div class="index-profit-item fin">
				<div class="index-profit-item-title">100% финансовая ответственность</div>
				<p>Мы несем полную финансовую ответственность за товар</p>
			</div>
		</div>
	</div>
</div>

<div class="index-route-block">
	<div class="index-route-block-container">
		<div class="index-route-block-map">
			<h2 class="index-block-header">Маршруты доставки грузов из Китая</h2>
			<div class="index-route-controls">
				<div class="index-route-auto-but" data-route="auto"></div>
				<div class="index-route-zd-but" data-route="zd"></div>
				<div class="index-route-sea-but" data-route="sea"></div>
			</div>
		</div>
		<div class="index-route-descs">
			<div class="index-route-desc" data-route="auto">
				<h3 class="index-route-desc-title">Автодоставка из Китая в Россию транзитом через Казахстан</h3>
				<div class="index-route-desc-lead">
					Мы работаем на маршруте доставки товаров из Китая через Казахстан.
					Мы осуществляем услугу доставки и таможенного оформления без привлечения посредников,
					что позволяет нашим клиентам получить короткие сроки доставки, отличную цену и полный комплекс услуг.
				</div>
				<a href="#" class="index-route-button" type="button">Подробнее</a>
			</div>
			<div class="index-route-desc" data-route="sea">
				<h3 class="index-route-desc-title">Доставка контейнеров из Китая морем</h3>
				<div class="index-route-desc-lead">
					Представляем Вашему вниманию контейнерный маршрут доставки морским транспортом.
					Доставка контейнерами через Дальний Восток в большинстве случаев является самым
					дешевым способом доставки грузов из Китая.
				</div>
				<a href="#" class="index-route-button" type="button">Подробнее</a>
			</div>
			<div class="index-route-desc" data-route="zd">
				<h3 class="index-route-desc-title">Ж/д доставка вагонов и контейнеров из Китая</h3>
				<div class="index-route-desc-lead">
					Железнодорожная доставка контей-нерами является надежным и недорогим способом
					доставить груз до Вашего города, заплатив совсем небольшую цену. Данный маршрут является более
					быстрой альтернативой маршруту контейнерной доставки морем через Дальний Восток.
					
				</div>
				<a href="#" class="index-route-button" type="button">Подробнее</a>
			</div>

		</div>
	</div>
</div>

<div class="index-services-block">
	<div class="shadow-bottom-block"></div>
	<div class="index-services-block-container">
		<h2 class="index-block-header-b">Наши услуги</h2>
		<div class="index-service index-services-deliver right">
			<h3 class="index-services-title">Доставка из Китая в Россию</h3>
			<div class="index-services-descr">
				Доставка грузов осуществляется из любого города в Китае в любой город России.
				В зависимости от потребностей Клиента Компания СибирьТрансАзия готова предложить
				наилучшие сроки и стоимость доставки. Вы получаете товар быстрее и выгоднее чем
				Ваши конкуренты и укрепляете позиции на рынке.
			</div>
			<a href="#" class="index-services-more">Подробнее</a>
		</div>
		<div class="index-service index-services-custom left">
			<h3 class="index-services-title">Таможенное оформление товаров</h3>
			<div class="index-services-descr">
				Профессионально-деловые отношения с таможенными органами позволяют осуществлять
				таможенные процедуры в кратчайшие сроки. С нашей помощью Вы забудете о корректировке
				таможенной стоимости (КТС), так как 90% ДТ выпускаются по 1 методу!
			</div>
			<a href="#" class="index-services-more">Подробнее</a>
		</div>
		<div class="index-service right">
			<h3 class="index-services-title">Логистическая обработка грузов</h3>
			<div class="index-services-descr">
				Весь товар, принимаемый к перевозке, проходит процедуру первичной логистической обработки.
				Наши специалисты контролируют все этапы перемещения груза и отвечают за безопасность товара на всем маршруте.
				Высокий профессионализм специалистов компании позволяет сократить сроки обработки грузов и соответственно
				увеличить скорость доставки товара.
			</div>
			<a href="#" class="index-services-more">Подробнее</a>
		</div>
		<div class="index-service left">
			<h3 class="index-services-title">Получение разрешительных документов</h3>
			<div class="index-services-descr">
				Специалисты по сертификации проведут анализ необходимости получения разрешительных документов для Вашего товара.
				С помощью Компании СибирьТрансАзия Вы сможете продавать товар соответствующий техническим регламентам Таможенного
				союза и не тратить свое время на сертификацию.
			</div>
			<a href="#" class="index-services-more">Подробнее</a>
		</div>
		<div class="index-service right">
			<h3 class="index-services-title">Представление Ваших интересов в Китае</h3>
			<div class="index-services-descr">
				Благодаря штату опытных специалистов по внешнеэкономической деятельности, Вы сможете найти необходимый товар в Китае.
				Мы проведем переговоры, проконтролируем выполнение договоренностей, достигнутых на этапе подписания договора.
			</div>
			<a href="#" class="index-services-more">Подробнее</a>
		</div>
		<div class="index-service index-services-legal left">
			<h3 class="index-services-title">Юридическое и финансовое сопровождение</h3>
			<div class="index-services-descr">
				Мы реализуем поставки как под ваш, так и под наш контракт с поставщиком.
				Задачу помогают решить опытные специалисты по международному праву, логистике, таможенному оформлению,
				валютным операциям и бухгалтерскому учету.
			</div>
			<a href="#" class="index-services-more">Подробнее</a>
		</div>
		
	</div>
</div>

<div class="index-work-scheme-block">
	<div class="index-work-scheme-inner">
		<h2 class="index-block-header-b">Как мы работаем?</h2>
		<ul class="index-work-scheme">
			<li>
				<i></i>
				<div class="index-work-scheme-icon lead"></div>
				<div class="index-work-scheme-item">Отправка заявки</div>
			</li>
			<li>
				<i></i>
				<div class="index-work-scheme-icon info"></div>
				<div class="index-work-scheme-item">Коммерческое предложение</div>
			</li>
			<li>
				<i></i>
				<div class="index-work-scheme-icon contract"></div>
				<div class="index-work-scheme-item">Заключение договора</div>
			</li>
			<li>
				<i></i>
				<div class="index-work-scheme-icon prepay"></div>
				<div class="index-work-scheme-item">Доставка в Россию</div>
			</li>
			<li>
				<i></i>
				<div class="index-work-scheme-icon deliver"></div>
				<div class="index-work-scheme-item">Таможенное оформление</div>
			</li>
			<li class="no-mr">
				<div class="index-work-scheme-icon postpay"></div>
				<div class="index-work-scheme-item">Получение товара</div>
			</li>
		</ul>
		<div class="index-work-scheme-video">
			<?$APPLICATION->IncludeFile(
				SITE_TEMPLATE_PATH.'/blocks/index.video.php',
				array(),
				array('MODE'=>'html','SHOW_BORDER'=>true)
			);?>
		</div>
	</div>
</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
