<?php
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$FeedName=htmlspecialchars($_REQUEST['FeedName']);
$FeedMail=htmlspecialchars($_REQUEST['FeedMail']);
$FeedPhone=htmlspecialchars($_REQUEST['FeedPhone']);
$FeedMsg=htmlspecialchars($_REQUEST['FeedMsg']);
$mess="Имя: ".$FeedName."\r\n";
$mess.="Телефон: ".$FeedPhone."\r\n";
if($FeedMail)
	$mess.="Эл. почта: ".$FeedMail."\r\n";
if($FeedMsg)
	$mess.="Сообщение: ".$FeedMsg."\r\n";
$arSend=array("TEXT"=>$mess);
CEvent::Send('FEEDBACK',SITE_ID,$arSend);
?>