<?
	if($_REQUEST['submit']){
		require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
		$APPLICATION->SetTitle("Спасибо за заказ | СибирьТрансАзия");
		
		$subjectSuffix = (isset($_REQUEST["subject"]) and !empty($_REQUEST["subject"]))? $_REQUEST["subject"] : "с калькулятора доставки";
		
		#$subject = "СТА: Заявка $subjectSuffix";
		$subject = '=?UTF-8?B?'.base64_encode("СТА: Заявка $subjectSuffix").'?=';  
		
		$mes = "Здравствуйте. На вашем сайте СТА была оставлена заявка $subjectSuffix:<br><br><br>";
		
		if(isset($_REQUEST['name']) and !empty($_REQUEST['name'])) {
			$name	= substr(htmlspecialchars(trim($_REQUEST['name'])), 0, 1000);
			$mes .= "Имя: " . $name . "<br>";
		}
		
		if(isset($_REQUEST['phone']) and !empty($_REQUEST['phone'])) {
			$phone = substr(htmlspecialchars(trim($_REQUEST['phone'])), 0, 1000000);
			$mes .= "Телефон: " . $phone . "<br>";
		}
		
		if(isset($_REQUEST['email']) and !empty($_REQUEST['email'])) {
			$email = substr(htmlspecialchars(trim($_REQUEST['email'])), 0, 1000000);
			$mes .= "Эл. почта: " . $email . "<br>";
		}
		
		if(isset($_REQUEST['from']) and !empty($_REQUEST['from'])) {
			$from = substr(htmlspecialchars(trim($_REQUEST['from'])), 0, 1000000);
			$mes .= "Доставка из " . $from . "<br>";
		}
		
		if(isset($_REQUEST['hiddenVolume']) and !empty($_REQUEST['hiddenVolume'])) {
			$hiddenVolume = substr(htmlspecialchars(trim($_REQUEST['hiddenVolume'])), 0, 1000000);
			$mes .= "Объем: " . $hiddenVolume . " куб. метров<br>";
		}
		
		if(isset($_REQUEST['hiddenWeight']) and !empty($_REQUEST['hiddenWeight'])) {
			$hiddenWeight = substr(htmlspecialchars(trim($_REQUEST['hiddenWeight'])), 0, 1000000);
			$mes .= "Вес: " . $hiddenWeight . " кг <br>";
		}
		
		if(isset($_REQUEST['totalUSD']) and !empty($_REQUEST['totalUSD'])) {
			$totalUSD = substr(htmlspecialchars(trim($_REQUEST['totalUSD'])), 0, 1000000);
			$mes .= "Сумма USD: " . $totalUSD . " $<br>";
		}
		
		if(isset($_REQUEST['totalRUB']) and !empty($_REQUEST['totalRUB'])) {
			$totalRUB = substr(htmlspecialchars(trim($_REQUEST['totalRUB'])), 0, 1000000);
			$mes .= "Сумма RUR: " . $totalRUB . " руб.<br>";
		}
		
		if(isset($_REQUEST['hiddenContract']) and !empty($_REQUEST['hiddenContract'])) {
			$hiddenContract = substr(htmlspecialchars(trim($_REQUEST['hiddenContract'])), 0, 1000000);
			$mes .= "Контракт: " . $hiddenContract . "<br>";
		}
		
		if(isset($_REQUEST['message']) and !empty($_REQUEST['message'])) {
			$message	=   substr(htmlspecialchars(trim($_REQUEST['message'])), 0, 10000000);
			$mes .= "Запрос: "  . $message . "<br>";
		}
		
		if(isset($_REQUEST['product']) and !empty($_REQUEST['product'])) {
			$productUrl	= substr(htmlspecialchars(trim($_REQUEST['product'])), 0, 1000);
			$productLink = "<a href=\"$productUrl\">$productUrl</a>";
			$mes .= "Товар: " . $productLink . "<br>";
		}
		# Проверяем загружен ли файл
		
		if(is_uploaded_file($_FILES["filename"]["tmp_name"][0])) {
			for($i = 0; $i < count($_FILES["filename"]["name"]); $i++) {
				if(is_uploaded_file($_FILES["filename"]["tmp_name"][$i])) {
					// Если файл загружен успешно, перемещаем его
					// из временной директории в конечную
					//echo '<img src="data:image/gif;base64,' . $file_str . '"/>';
					//$targetFolder = 'images/uploads'; // Relative to the root
					//$targetPath = $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/" . $targetFolder;
					
					$file_src = $_FILES["filename"]["tmp_name"][$i];
					$file = fread(fopen($file_src, "r"), filesize($file_src));
					$file_str = base64_encode($file);
					
					//move_uploaded_file($_FILES["filename"]["tmp_name"][$i], "upload/".$_FILES["filename"]["name"][$i]);
					//move_uploaded_file($_FILES["filename"]["tmp_name"][$i], $targetPath);
					$arr[] = array(
						"type" 		=> $_FILES["filename"]["type"][$i],
						"name" 		=> $_FILES["filename"]["name"][$i],
						"content"	=> $file_str
					);
				} else {
					echo("Ошибка загрузки файла");
				}
			}
		}
		echo "<h1>";
		if(multipart_mail("no-reply@sibtransasia.ru", "orders@sibtransasia.ru", $subject, $mes, $cc=null, $arr)){
			echo "Спасибо за заявку. Мы свяжемся с Вами в ближайшее время.";
		} else {
			echo "Ошибка. Попробуйте еще раз или напишите об этом на info@sibtransasia.ru";
		}
		echo "</h1>";
		/*
		$arSend=array("TEXT"=>$mes);
		
		echo "<h1>";
			//if(multipart_mail("no-reply@sibtransasia.ru", "kps@sibtransasia.ru", $description, $mes, null, null)) {
			if(CEvent::Send('CALLBACK',SITE_ID,$arSend)) {
				echo "Спасибо за заявку. Мы свяжемся с Вами в ближайшее время.";
			} else {
				echo "Ошибка. Попробуйте еще раз или напишите об этом на info@sibtransasia.ru";
			}
		echo "</h1>";*/
	} else {
		header("Location: /");
	}

	/**
	* отправка письма с вложениями
	*
	* @param string $from
	* @param string $to
	
	* @param string $subject
	* @param string $text
	* @param string $cc
	* @param string $arr массив ассоциативных массивов с прикладываемыми файлами
	
	*     $arr[] = array( 
			   type = тип mime,
			   name = имя файла (лучше английское),
			   content = содержимое файла закодированное  base64_encode() 
	
		   )
	
	* @return unknown
	*/
	
	function multipart_mail($from, $to, $subject, $text,$cc=null,$arr=null) {
	
		global $domain;
		if($domain=='') $domain='http://'.$_SERVER['HTTP_HOST'];
		$headers ="From: $from\r\n";
	
		$headers.="To: $to\r\n";
		if (!is_null($cc))     {
		  $headers.="Cc: $cc\r\n";
	
		}
		$headers.="Subject: $subject\r\n";
		$headers.="Date: ".date("r")."\r\n";
	
		$headers.="X-Mailer: zm php script\r\n";
		$headers.="MIME-Version: 1.0\r\n";
		$headers.="Content-Type: multipart/alternative;\r\n";
	
		$baseboundary="------------".strtoupper(md5(uniqid(rand(), true)));
		$headers.="  boundary=\"$baseboundary\"\r\n";
	
		$headers.="This is a multi-part message in MIME format.\r\n";
		$message="--$baseboundary\r\n";
		$message.="Content-Type: text/plain; charset=utf-8\r\n";
	
		$text_plain=str_replace('<p>',"\r\n",$text);
		$text_plain=str_replace('<b>',"",$text_plain);
	
		$text_plain=str_replace('</b>',"",$text_plain);
		$text_plain=str_replace('<br>',"\r\n",$text_plain);
	
		$text_plain= preg_replace('/<a(\s+)href="([^"]+)"([^>]+)>([^<]+)/i'," \$4\n\$2",$text_plain);
		$message.=strip_tags($text_plain);
	
		//$message.="Content-Transfer-Encoding: 7bit\r\n\r\n";
		$message.="\r\n\r\nIts simple text. Switch to HTML view!\r\n\r\n";
		$message.="--$baseboundary\r\n";
		$newboundary="------------".strtoupper(md5(uniqid(rand(), true)));
	
		$message.="Content-Type: multipart/related;\r\n";
		$message.="  boundary=\"$newboundary\"\r\n\r\n\r\n";
		$message.="--$newboundary\r\n";
		$message.="Content-Type: text/html; charset=utf-8\r\n";
	
		$message.="Content-Transfer-Encoding: 7bit\r\n\r\n";
		$message.=($text)."\r\n\r\n";
		preg_match_all('/img(\s+)src="([^"]+)"/i',$text,$m);

		if (isset($m[2])) {
			$img_f=$m[2];
			if (is_array($img_f)) {
				foreach ($img_f as $k => $v) {
					$img_f[$k]=str_ireplace($domain,$_SERVER['DOCUMENT_ROOT'],$v);
				}
			}
		}
		$attachment_files=$img_f;
		
		if (is_array($attachment_files)) {
			foreach($attachment_files as $filename)  {
	
				$file_content = file_get_contents($filename,true);
				$mime_type='image/png';
				if(function_exists("mime_content_type"))  {
	
					$mime_type=mime_content_type($filename);    
				}
				else {
						switch (file_ext($filename))    {
		
						case 'jpg': $mime_type='image/jpeg';break;
						case 'gif': $mime_type='image/gif';break;
						case 'png': $mime_type='image/png';break;
	   
						default:;
					}
				}
				$message=str_replace($domain.'/'.$filename,'cid:'.basename($filename),$message);

				$filename=basename($filename);
				$message.="--$newboundary\r\n";
				$message.="Content-Type: $mime_type;\r\n";
				$message.=" name=\"$filename\"\r\n";

				$message.="Content-Transfer-Encoding: base64\r\n";
				$message.="Content-ID: <$filename>\r\n";
				$message.="Content-Disposition: inline;\r\n";
				$message.=" filename=\"$filename\"\r\n\r\n";

				$message.=chunk_split(base64_encode($file_content));
			}
			if(!is_null($arr)){
				foreach ($arr as $v) {
					$mime_type=$v['type'];
					$filename=$v['name'];
					   $message.="--$newboundary\r\n";
					   $message.="Content-Type: $mime_type;\r\n";
	   
					   $message.=" name=\"$filename\"\r\n";
					   $message.="Content-Transfer-Encoding: base64\r\n";
					   $message.="Content-ID: <$filename>\r\n";
					   $message.="Content-Disposition: inline;\r\n";
	   
					   $message.=" filename=\"$filename\"\r\n\r\n";
					   $message.=chunk_split($v['content']);
				}
			}

		}
		$message.="--$newboundary--\r\n\r\n";
		$message.="--$baseboundary--\r\n";

		return @mail($to, $subject, $message , $headers);
	}
?>
<script>
	//window.location.href="http://sibtransasia.ru"
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>