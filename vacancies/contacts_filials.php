<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
$APPLICATION->SetPageProperty("description", "Контакты филиалов компании СибирьТрансАзиа. Доставка грузов из Китая");
$APPLICATION->SetPageProperty("keywords", "СибирьТрансАзиа контакты, sibtransasia контакты, СибирьТрансАзия контакты филиалов");
?> 
<p id="contacts_intro">Наша компания имеет представительства в трех городах России, а также представительство в Урумчи, КНР</p>
 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"contacts",
	Array(
		"IBLOCK_TYPE" => "contacts",
		"IBLOCK_ID" => "8",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"PROPERTY_CODE" => array(0=>"ADDRESS",1=>"MAP",2=>"COUNTRY",3=>"PHONE",4=>"TYPE",5=>"EMAIL",6=>"",),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_OPTION_ADDITIONAL" => ""
	)
);?> <?$APPLICATION->IncludeFile(
	SITE_TEMPLATE_PATH.'/_include_areas_/contacts.feedback.php',
	array(),
	array('MODE'=>'php','SHOW_BORDER'=>false)
);?> 
<br />
 <hr> 
<br />
 
<div> 
  <br />
 
  <table width="100%" border="0" cellpadding="5" cellspacing="5" align="left"> 
    <tbody> 
      <tr><td>
<div><strong>Реквизиты компании &quot;СибирьТрансАзия&quot;</strong></div><div><span style="line-height: 1.538em;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span></div><div><strong>ООО &quot;СибирьТрансАзия&quot; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</strong></div><div>Юридический адрес: &nbsp;658248, Россия, Алтайский край, Рубцовский район, с. Веселоярск, ул. Ленина, &nbsp;211</div><div>ИНН: &nbsp;2269009204\КПП: &nbsp; &nbsp;226901001&nbsp;</div><div>ОГРН: &nbsp; &nbsp; 1082209000378 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div><div>Расчетный счет: &nbsp; 40702810903000002050</div><div>Валютный счет: &nbsp;40702840300200001385</div><div>Банк: &nbsp;БАНК &quot;ЛЕВОБЕРЕЖНЫЙ&quot; (ОАО), г. Новосибирск</div><div>БИК: 045004850</div><div>Кор.счет: &nbsp; 30101810100000000850</div><div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div><div><strong>Директор ООО &laquo;СибирьТрасАзия&raquo; &nbsp; &nbsp; &nbsp; </strong></div><div><strong>Грищенко Игорь Николаевич</strong></div><div>&nbsp;</div><div><a href="http://www.sibtransasia.ru/contacts/"><strong>&lt;&lt;&lt; Назад&nbsp;</strong></a></div><div>&nbsp;</div>

</td></tr>
     </tbody>
   </table>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>